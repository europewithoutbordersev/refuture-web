package eu.refuture.rest.json;

import java.util.Collection;
import java.util.List;

import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;

import com.google.common.base.Strings;
import com.google.common.collect.Lists;

import eu.refuture.model.news.News;
import eu.refuture.model.news.NewsLocalization;

/**
 * Generates JSON for News
 * 
 * @author Europe Without Borders e.V.
 * @version 1.0
 */
public class NewsJsonGenerator {

	public static String generateJsonAsStringForNews(Collection<News> newsCollection, String... langs) {
		JsonArrayBuilder newsJsonArrayBuilder = Json.createArrayBuilder();
		for (News news : newsCollection) {
			newsJsonArrayBuilder.add(generateJsonForNews(news, langs));
		}

		JsonObjectBuilder newsCollectionJsonObjectBuilder = Json.createObjectBuilder();
		newsCollectionJsonObjectBuilder = newsCollectionJsonObjectBuilder.add("news", newsJsonArrayBuilder);
		return newsCollectionJsonObjectBuilder.build().toString();
	}

	private static JsonObject generateJsonForNews(News news, String... langs) {
		List<String> languages = null;
		if (langs != null) {
			languages = Lists.newArrayList(langs);
		}

		JsonObjectBuilder newsJsonObjectBuilder = Json.createObjectBuilder();
		// id
		newsJsonObjectBuilder = newsJsonObjectBuilder.add("id", news.getId());
		if (news.getLatitude() != 0 || news.getLongitude() != 0) {
			// lat long
			newsJsonObjectBuilder = newsJsonObjectBuilder.add("latitude", news.getLatitude());
			newsJsonObjectBuilder = newsJsonObjectBuilder.add("longitude", news.getLongitude());
		} else {
			if (news.getCountry() != null && !Strings.isNullOrEmpty(news.getCountry().getId())) {
				// country
				newsJsonObjectBuilder = newsJsonObjectBuilder.add("country_code", news.getCountry().getId());
			}
		}
		// timestamp
		newsJsonObjectBuilder = newsJsonObjectBuilder.add("timestamp", news.getCreatedAt().getTime());
		// expiration
		if (news.getExpirationDate() != null) {
			newsJsonObjectBuilder = newsJsonObjectBuilder.add("expiration", news.getExpirationDate().getTime());
		}
		// localizations
		if (languages != null) {
			JsonArrayBuilder localizationsJsonArrayBuilder = Json.createArrayBuilder();
			for (NewsLocalization localization : news.getLocalizations()) {
				if (languages.contains(localization.getLanguage())) {
					JsonObjectBuilder newsLocalizationJsonObjectBuilder = Json.createObjectBuilder();
					// id
					newsLocalizationJsonObjectBuilder = newsLocalizationJsonObjectBuilder.add("id",
							localization.getId());
					// language
					newsLocalizationJsonObjectBuilder = newsLocalizationJsonObjectBuilder.add("language",
							localization.getLanguage());
					// heading
					newsLocalizationJsonObjectBuilder = newsLocalizationJsonObjectBuilder.add("heading",
							localization.getHeading());
					// body
					newsLocalizationJsonObjectBuilder = newsLocalizationJsonObjectBuilder.add("body",
							localization.getBody());

					localizationsJsonArrayBuilder.add(newsLocalizationJsonObjectBuilder);
				}
			}

			newsJsonObjectBuilder = newsJsonObjectBuilder.add("localizations", localizationsJsonArrayBuilder);
		}

		return newsJsonObjectBuilder.build();
	}

}
