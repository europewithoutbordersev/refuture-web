package eu.refuture.rest.json;

import java.util.Collection;

import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;

import eu.refuture.model.country.Country;
import eu.refuture.model.country.CountryLocalization;

/**
 * Generates JSON for Countries
 * 
 * @author Europe Without Borders e.V.
 * @version 1.0
 */
public class CountryJsonGenerator {

	public static String getJsonAsStringForCountries(Collection<Country> countries) {

		JsonArrayBuilder countriesJsonArrayBuilder = Json.createArrayBuilder();
		for (Country country : countries) {
			countriesJsonArrayBuilder.add(getJsonObjectForCountry(country));
		}

		JsonObjectBuilder countriesJsonObjectBuilder = Json.createObjectBuilder();
		countriesJsonObjectBuilder = countriesJsonObjectBuilder.add("countries", countriesJsonArrayBuilder);
		return countriesJsonObjectBuilder.build().toString();
	}

	private static JsonObject getJsonObjectForCountry(Country country) {
		JsonObjectBuilder countryJsonObjectBuilder = Json.createObjectBuilder();
		// country code
		countryJsonObjectBuilder = countryJsonObjectBuilder.add("country_code", country.getId());

		JsonArrayBuilder localizationsJsonArrayBuilder = Json.createArrayBuilder();
		for (CountryLocalization localization : country.getLocalizations()) {
			JsonObjectBuilder countryLocalizationJsonObjectBuilder = Json.createObjectBuilder();
			// language
			countryLocalizationJsonObjectBuilder = countryLocalizationJsonObjectBuilder.add("lang",
					localization.getLang());
			// name
			countryLocalizationJsonObjectBuilder = countryLocalizationJsonObjectBuilder.add("name",
					localization.getName());

			localizationsJsonArrayBuilder.add(countryLocalizationJsonObjectBuilder);
		}

		countryJsonObjectBuilder = countryJsonObjectBuilder.add("localizations", localizationsJsonArrayBuilder);

		return countryJsonObjectBuilder.build();
	}

}
