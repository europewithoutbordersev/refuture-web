package eu.refuture.rest.json;

import javax.json.Json;
import javax.json.JsonObjectBuilder;

/**
 * Generates JSON for refugee_ids
 * 
 * @author Europe Without Borders e.V.
 * @version 1.0
 */
public class RefugeeIdJsonGenerator {

	public static String getJsonAsStringForRefugeeId(long id) {
		JsonObjectBuilder refIdJsonObjectBuilder = Json.createObjectBuilder();
		refIdJsonObjectBuilder = refIdJsonObjectBuilder.add("refugee_id", id);

		return refIdJsonObjectBuilder.build().toString();
	}

}
