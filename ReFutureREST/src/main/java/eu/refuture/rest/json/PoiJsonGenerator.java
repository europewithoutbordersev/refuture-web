package eu.refuture.rest.json;

import java.util.Collection;
import java.util.List;

import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;

import com.google.common.collect.Lists;

import eu.refuture.model.poi.PointOfInterest;
import eu.refuture.model.poi.PointOfInterestLocalization;
import eu.refuture.model.poi.PointOfInterestTypeLocalization;

/**
 * Generates JSON for POIs
 * 
 * @author Europe Without Borders e.V.
 * @version 1.0
 */
public class PoiJsonGenerator {

	public static String generateJsonAsStringForPois(Collection<PointOfInterest> pois, String... langs) {
		JsonArrayBuilder localizationsJsonArrayBuilder = Json.createArrayBuilder();

		for (PointOfInterest poi : pois) {
			JsonObject poiJsonObject = generateJsonForPoi(poi, langs);
			localizationsJsonArrayBuilder = localizationsJsonArrayBuilder.add(poiJsonObject);
		}

		JsonObjectBuilder poisJsonObjectBuilder = Json.createObjectBuilder();
		poisJsonObjectBuilder = poisJsonObjectBuilder.add("locations", localizationsJsonArrayBuilder);

		return poisJsonObjectBuilder.build().toString();
	}

	public static String generateJsonAsStringForPoi(PointOfInterest poi, String... langs) {
		return generateJsonForPoi(poi, langs).toString();
	}

	public static JsonObject generateJsonForPoi(PointOfInterest poi, String... langs) {
		List<String> languages = null;
		if (langs != null) {
			languages = Lists.newArrayList(langs);
		}
		JsonObjectBuilder poiJsonObjectBuilder = Json.createObjectBuilder();
		poiJsonObjectBuilder = poiJsonObjectBuilder.add("id", poi.getId());
		poiJsonObjectBuilder = poiJsonObjectBuilder.add("latitude", poi.getLatitude());
		poiJsonObjectBuilder = poiJsonObjectBuilder.add("longitude", poi.getLongitude());
		if (poi.getRatio() >= 0) {
			poiJsonObjectBuilder = poiJsonObjectBuilder.add("ratio", poi.getRatio());
		}

		// poi localizations for given languages
		if (languages != null) {
			JsonArrayBuilder localizationsJsonArrayBuilder = Json.createArrayBuilder();

			for (PointOfInterestLocalization localization : poi.getLocalizations()) {
				if (languages.contains(localization.getLanguage())) {
					JsonObjectBuilder poiLocalizationJsonObjectBuilder = Json.createObjectBuilder();
					// id
					poiLocalizationJsonObjectBuilder = poiLocalizationJsonObjectBuilder.add("id", localization.getId());
					// language
					poiLocalizationJsonObjectBuilder = poiLocalizationJsonObjectBuilder.add("language",
							localization.getLanguage());
					// name
					poiLocalizationJsonObjectBuilder = poiLocalizationJsonObjectBuilder.add("name",
							localization.getName());
					// description
					poiLocalizationJsonObjectBuilder = poiLocalizationJsonObjectBuilder.add("description",
							localization.getDescription());
					// capacity
					if (localization.getCapacity() != null) {
						poiLocalizationJsonObjectBuilder = poiLocalizationJsonObjectBuilder.add("capacity",
								localization.getCapacity());
					}

					localizationsJsonArrayBuilder = localizationsJsonArrayBuilder.add(poiLocalizationJsonObjectBuilder);
				}
			}

			poiJsonObjectBuilder = poiJsonObjectBuilder.add("localizations", localizationsJsonArrayBuilder);
		}

		// type
		if (poi.getType() != null) {
			poiJsonObjectBuilder = poiJsonObjectBuilder.add("type", poi.getType().getId());

			if (languages != null) {
				JsonArrayBuilder typeLocalizationsJsonArrayBuilder = Json.createArrayBuilder();

				for (PointOfInterestTypeLocalization poiTypeLocalization : poi.getType().getLocalizations()) {
					if (languages.contains(poiTypeLocalization.getLang())) {
						JsonObjectBuilder typeLocalizationJsonObjectBuilder = Json.createObjectBuilder();
						// language
						typeLocalizationJsonObjectBuilder = typeLocalizationJsonObjectBuilder.add("language",
								poiTypeLocalization.getLang());
						// name
						typeLocalizationJsonObjectBuilder = typeLocalizationJsonObjectBuilder.add("name",
								poiTypeLocalization.getName());
						// desc
						if (poiTypeLocalization.getDescription() != null) {
							typeLocalizationJsonObjectBuilder = typeLocalizationJsonObjectBuilder.add("description",
									poiTypeLocalization.getDescription());
						}

						typeLocalizationsJsonArrayBuilder = typeLocalizationsJsonArrayBuilder
								.add(typeLocalizationJsonObjectBuilder);
					}
				}

				poiJsonObjectBuilder = poiJsonObjectBuilder.add("type_localizations",
						typeLocalizationsJsonArrayBuilder);
			}
		}

		return poiJsonObjectBuilder.build();
	}

}
