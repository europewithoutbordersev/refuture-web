package eu.refuture.rest;

import java.io.InputStream;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.google.common.collect.Lists;

import eu.refuture.dbaccess.DatabaseAccess;
import eu.refuture.dbaccess.IDatabaseAccess;
import eu.refuture.model.news.News;
import eu.refuture.rest.json.NewsJsonGenerator;

/**
 * Rest Resource for News
 * 
 * @author Europe Without Borders e.V.
 * @version 1.0
 */
@Path("/news")
public class NewsResource {

	private IDatabaseAccess dbAccess = new DatabaseAccess();

	@POST
	@Path("{id}")
	@Consumes({ MediaType.TEXT_PLAIN, MediaType.APPLICATION_JSON })
	public Response getNewsById(@PathParam("id") long id, InputStream jsonNewsRequest) {
		System.out.println("id: " + id);
		Response response = null;
		try {
			JsonReader rdr = Json.createReader(jsonNewsRequest);
			JsonObject obj = rdr.readObject();
			long refugeeId = obj.getJsonNumber("refugee_id").longValue();

			if (dbAccess.validateRefugeeId(refugeeId)) {
				String[] requestLanguages = extractRequestLanguages(obj);

				News news = dbAccess.getNewsByIdAndLanguages(id, requestLanguages);

				if (news != null) {
					Collection<News> newsCollection = Lists.newArrayList(news);
					response = Response.ok()
							.entity(NewsJsonGenerator.generateJsonAsStringForNews(newsCollection, requestLanguages))
							.type(MediaType.APPLICATION_JSON).build();
				} else {
					response = Response.status(Status.NOT_FOUND).build();
				}
			} else {
				response = Response.status(Status.UNAUTHORIZED).build();
			}

		} catch (Exception e) {
			e.printStackTrace();
			response = Response.status(Status.INTERNAL_SERVER_ERROR).build();
		}
		return response;
	}

	@POST
	@Consumes({ MediaType.TEXT_PLAIN, MediaType.APPLICATION_JSON })
	public Response getAllNews(InputStream jsonNewsRequest) {
		Response response = null;
		try {
			JsonReader rdr = Json.createReader(jsonNewsRequest);
			JsonObject obj = rdr.readObject();
			long refugeeId = obj.getJsonNumber("refugee_id").longValue();

			if (dbAccess.validateRefugeeId(refugeeId)) {
				String[] requestLanguages = extractRequestLanguages(obj);

				Date newerThanDate = null;
				if (obj.getJsonNumber("timestamp") != null) {
					newerThanDate = new Date(obj.getJsonNumber("timestamp").longValue());
				}

				List<News> newsCollection = dbAccess.getNews(newerThanDate, requestLanguages);

				if (newsCollection == null || newsCollection.isEmpty()) {
					// found nothing
					response = Response.status(Status.NOT_FOUND).build();
				} else {
					response = Response.ok()
							.entity(NewsJsonGenerator.generateJsonAsStringForNews(newsCollection, requestLanguages))
							.type(MediaType.APPLICATION_JSON).build();
				}
			} else {
				response = Response.status(Status.UNAUTHORIZED).build();
			}

		} catch (Exception e) {
			e.printStackTrace();
			response = Response.status(Status.INTERNAL_SERVER_ERROR).build();
		}
		return response;
	}

	private String[] extractRequestLanguages(JsonObject obj) {
		List<String> languages = Lists.newArrayList();
		JsonArray array = obj.getJsonArray("languages");
		for (int i = 0; i < array.size(); i++) {
			System.out.println(array.get(i));
			languages.add(array.get(i).toString().replace("\"", ""));
		}
		String[] langsArray = new String[languages.size()];
		langsArray = languages.toArray(langsArray);
		return langsArray;
	}
}
