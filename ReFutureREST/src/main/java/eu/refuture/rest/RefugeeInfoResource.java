package eu.refuture.rest;

import java.io.InputStream;
import java.util.List;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.google.common.collect.Lists;

import eu.refuture.dbaccess.DatabaseAccess;
import eu.refuture.dbaccess.IDatabaseAccess;
import eu.refuture.model.refugee.Refugee;
import eu.refuture.model.refugee.RefugeeLocalization;
import eu.refuture.rest.json.RefugeeIdJsonGenerator;

/**
 * Rest Resource for Refugee information
 * 
 * @author Europe Without Borders e.V.
 * @version 1.0
 */
@Path("/register")
public class RefugeeInfoResource {

	private IDatabaseAccess dbAccess = new DatabaseAccess();

	@POST
	@Consumes({ MediaType.TEXT_PLAIN, MediaType.APPLICATION_JSON })
	public Response getPoiById(InputStream jsonNewsRequest) {
		Response response = null;
		try {
			JsonReader rdr = Json.createReader(jsonNewsRequest);
			JsonObject obj = rdr.readObject();

			Refugee refugee = new Refugee();

			String startCountryCode = obj.getString("start_country_code").toUpperCase();
			refugee.setStartCountry(dbAccess.getCountryById(startCountryCode));

			String stopCountryCode = obj.getString("stop_country_code").toUpperCase();
			refugee.setTargetCountry(dbAccess.getCountryById(stopCountryCode));

			String[] requestLanguages = extractRequestLanguages(obj);
			for (String lang : requestLanguages) {
				RefugeeLocalization localization = new RefugeeLocalization();
				localization.setRefugee(refugee);
				localization.setLanguage(lang);
				refugee.addLocalization(localization);
			}

			int influx = obj.getJsonNumber("influx").intValue();
			refugee.setFamilyMembersFollowing(influx);

			int entourage = obj.getJsonNumber("entourage").intValue();
			refugee.setEntourage(entourage);

			int age = obj.getJsonNumber("age").intValue();
			refugee.setAge(age);

			boolean isMale = obj.getBoolean("is_male");
			char gender;
			if (isMale) {
				gender = 'm';
			} else {
				gender = 'w';
			}
			refugee.setGender(gender);

			int educationId = obj.getJsonNumber("educationId").intValue();
			refugee.setEducation("education id: " + educationId); // TODO

			if (obj.containsKey("refugee_id")) {
				// update
				long refugeeId = obj.getJsonNumber("refugee_id").longValue();
				refugee.setId(refugeeId);
				dbAccess.updateRefugee(refugee);
				response = Response.ok().build();
			} else {
				// new refugee
				System.out.println("new");
				long newRefugeeId = dbAccess.saveNewRefugee(refugee);
				response = Response.ok().entity(RefugeeIdJsonGenerator.getJsonAsStringForRefugeeId(newRefugeeId))
						.type(MediaType.APPLICATION_JSON).build();
			}
		} catch (Exception e) {
			e.printStackTrace();
			response = Response.status(Status.INTERNAL_SERVER_ERROR).build();
		}
		return response;
	}

	private String[] extractRequestLanguages(JsonObject obj) {
		List<String> languages = Lists.newArrayList();
		JsonArray array = obj.getJsonArray("languages");
		for (int i = 0; i < array.size(); i++) {
			System.out.println(array.get(i));
			languages.add(array.get(i).toString().replace("\"", ""));
		}
		String[] langsArray = new String[languages.size()];
		langsArray = languages.toArray(langsArray);
		return langsArray;
	}

}
