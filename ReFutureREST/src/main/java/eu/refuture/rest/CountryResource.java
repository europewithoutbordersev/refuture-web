package eu.refuture.rest;

import java.util.Collection;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import eu.refuture.dbaccess.DatabaseAccess;
import eu.refuture.dbaccess.IDatabaseAccess;
import eu.refuture.model.country.Country;
import eu.refuture.rest.json.CountryJsonGenerator;

/**
 * Rest Resource for Countries
 * 
 * @author Europe Without Borders e.V.
 * @version 1.0
 */
@Path("/countries")
public class CountryResource {

	private IDatabaseAccess dbAccess = new DatabaseAccess();

	@GET
	public Response getNewsById() {
		Response response = null;
		try {
			Collection<Country> countryCollection = dbAccess.getCountries();

			if (countryCollection == null || countryCollection.isEmpty()) {
				// nothing found
				response = Response.status(Status.NOT_FOUND).build();
			} else {
				response = Response.ok().entity(CountryJsonGenerator.getJsonAsStringForCountries(countryCollection))
						.type(MediaType.APPLICATION_JSON).build();
			}
		} catch (Exception e) {
			e.printStackTrace();
			response = Response.status(Status.INTERNAL_SERVER_ERROR).build();
		}
		return response;
	}

}
