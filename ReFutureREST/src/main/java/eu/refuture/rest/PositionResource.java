package eu.refuture.rest;

import java.io.InputStream;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import eu.refuture.dbaccess.DatabaseAccess;
import eu.refuture.dbaccess.IDatabaseAccess;

/**
 * Rest Resource for saving Rafugee-Positions
 * 
 * @author Europe Without Borders e.V.
 * @version 1.0
 */
@Path("/position")
public class PositionResource {

	private IDatabaseAccess dbAccess = new DatabaseAccess();

	@POST
	@Consumes({ MediaType.TEXT_PLAIN, MediaType.APPLICATION_JSON })
	public Response savePosition(InputStream jsonNewsRequest) {
		Response response = null;
		try {
			JsonReader rdr = Json.createReader(jsonNewsRequest);
			JsonObject obj = rdr.readObject();
			long refugeeId = obj.getJsonNumber("refugee_id").longValue();

			double latitude = obj.getJsonNumber("latitude").doubleValue();
			double longitude = obj.getJsonNumber("longitude").doubleValue();

			dbAccess.savePosition(refugeeId, latitude, longitude);
			response = Response.ok().build();

		} catch (Exception e) {
			e.printStackTrace();
			response = Response.status(Status.INTERNAL_SERVER_ERROR).build();
		}
		return response;
	}
}
