package eu.refuture.rest;

import java.io.InputStream;
import java.util.Date;
import java.util.List;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.google.common.collect.Lists;

import eu.refuture.dbaccess.DatabaseAccess;
import eu.refuture.dbaccess.IDatabaseAccess;
import eu.refuture.model.poi.PointOfInterest;
import eu.refuture.rest.json.PoiJsonGenerator;

/**
 * Rest Resource for POIs
 * 
 * @author Europe Without Borders e.V.
 * @version 1.0
 */
@Path("/poi")
public class PoiResource {

	private IDatabaseAccess dbAccess = new DatabaseAccess();

	@POST
	@Path("{id}")
	@Consumes({ MediaType.TEXT_PLAIN, MediaType.APPLICATION_JSON })
	public Response getPoiById(@PathParam("id") long id, InputStream jsonNewsRequest) {
		Response response = null;
		try {
			JsonReader rdr = Json.createReader(jsonNewsRequest);
			JsonObject obj = rdr.readObject();
			long refugeeId = obj.getJsonNumber("refugee_id").longValue();

			if (dbAccess.validateRefugeeId(refugeeId)) {
				String[] requestLanguages = extractRequestLanguages(obj);

				PointOfInterest poi = dbAccess.getPoiById(id);

				if (poi == null) {
					// found nothing
					response = Response.status(Status.NOT_FOUND).build();
				} else {
					List<PointOfInterest> pois = Lists.newArrayList(poi);
					response = Response.ok()
							.entity(PoiJsonGenerator.generateJsonAsStringForPois(pois, requestLanguages))
							.type(MediaType.APPLICATION_JSON).build();
				}
			} else {
				response = Response.status(Status.UNAUTHORIZED).build();
			}
		} catch (Exception e) {
			e.printStackTrace();
			response = Response.status(Status.INTERNAL_SERVER_ERROR).build();
		}
		return response;

	}

	@POST
	@Consumes({ MediaType.TEXT_PLAIN, MediaType.APPLICATION_JSON })
	public Response getAllPois(InputStream jsonNewsRequest) {
		Response response = null;
		try {
			JsonReader rdr = Json.createReader(jsonNewsRequest);
			JsonObject obj = rdr.readObject();
			long refugeeId = obj.getJsonNumber("refugee_id").longValue();

			if (dbAccess.validateRefugeeId(refugeeId)) {
				Date newerThanDate = null;
				if (obj.getJsonNumber("timestamp") != null) {
					newerThanDate = new Date(obj.getJsonNumber("timestamp").longValue());
				}

				String[] requestLanguages = extractRequestLanguages(obj);

				List<PointOfInterest> poiCollection = dbAccess.getPois(newerThanDate);

				if (poiCollection == null || poiCollection.isEmpty()) {
					// found nothing
					response = Response.status(Status.NOT_FOUND).build();
				} else {
					response = Response.ok()
							.entity(PoiJsonGenerator.generateJsonAsStringForPois(poiCollection, requestLanguages))
							.type(MediaType.APPLICATION_JSON).build();
				}
			} else {
				response = Response.status(Status.UNAUTHORIZED).build();
			}
		} catch (Exception e) {
			e.printStackTrace();
			response = Response.status(Status.INTERNAL_SERVER_ERROR).build();
		}
		return response;
	}

	private String[] extractRequestLanguages(JsonObject obj) {
		List<String> languages = Lists.newArrayList();
		JsonArray array = obj.getJsonArray("languages");
		for (int i = 0; i < array.size(); i++) {
			System.out.println(array.get(i));
			languages.add(array.get(i).toString().replace("\"", ""));
		}
		// while(langIterator.hasNext()){
		// JsonValue value = langIterator.next();
		// value.
		// System.out.println(value.toString());
		// languages.add(value.toString());
		// }
		String[] langsArray = new String[languages.size()];
		langsArray = languages.toArray(langsArray);
		return langsArray;
	}

}
