package eu.refuture.rest.json;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * @author Europe Without Borders e.V.
 * @version 1.0
 */
public class RefugeeIdJsonGeneratorTest {

	@Test
	public void testGetJsonAsStringForRefugeeId() {
		String expectedResult = "{\"refugee_id\":1337}";

		long id = 1337;

		String result = RefugeeIdJsonGenerator.getJsonAsStringForRefugeeId(id);
		System.out.println(result);
		assertEquals(expectedResult, result);
	}

}
