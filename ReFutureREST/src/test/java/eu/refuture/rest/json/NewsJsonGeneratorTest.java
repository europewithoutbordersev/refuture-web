package eu.refuture.rest.json;

import static org.junit.Assert.*;

import java.util.Collection;
import java.util.Date;

import org.junit.Test;

import com.google.common.collect.Lists;

import eu.refuture.model.country.Country;
import eu.refuture.model.news.News;
import eu.refuture.model.news.NewsLocalization;

/**
 * @author Europe Without Borders e.V.
 * @version 1.0
 */
public class NewsJsonGeneratorTest {

	@Test
	public void testGenerateJsonForNews() {
		String expectedResult = "{\"news\":[{\"id\":1,\"timestamp\":4711,\"localizations\":[{\"id\":1,\"language\":\"en\",\"heading\":\"test\",\"body\":\"this is a test message\"}]}]}";

		News news = new News();
		news.setCreatedAt(new Date(4711));
		news.setId(1);
		NewsLocalization localization = new NewsLocalization();
		localization.setId(1);
		localization.setNews(news);
		localization.setLanguage("en");
		localization.setHeading("test");
		localization.setBody("this is a test message");
		news.addLocalization(localization);

		Collection<News> newsCollection = Lists.newArrayList(news);
		String result = NewsJsonGenerator.generateJsonAsStringForNews(newsCollection, "en");
		assertEquals(expectedResult, result);
	}

	@Test
	public void testGenerateJsonForNewsCountry() {
		String expectedResult = "{\"news\":[{\"id\":1,\"country_code\":\"DE\",\"timestamp\":4711,\"localizations\":[{\"id\":1,\"language\":\"en\",\"heading\":\"test\",\"body\":\"this is a test message\"}]}]}";

		Country country = new Country();
		country.setId("DE");

		News news = new News();
		news.setCreatedAt(new Date(4711));
		news.setId(1);
		news.setCountry(country);
		NewsLocalization localization = new NewsLocalization();
		localization.setNews(news);
		localization.setId(1);
		localization.setLanguage("en");
		localization.setHeading("test");
		localization.setBody("this is a test message");
		news.addLocalization(localization);

		Collection<News> newsCollection = Lists.newArrayList(news);
		String result = NewsJsonGenerator.generateJsonAsStringForNews(newsCollection, "en");
		assertEquals(expectedResult, result);
	}

	@Test
	public void testGenerateJsonForNewsLatLong() {
		String expectedResult = "{\"news\":[{\"id\":1,\"latitude\":13.37,\"longitude\":47.11,\"timestamp\":0,\"localizations\":[{\"id\":1,\"language\":\"en\",\"heading\":\"test\",\"body\":\"this is a test message\"}]}]}";

		News news = new News();
		news.setCreatedAt(new Date(0));
		news.setId(1);
		news.setLatitude(13.37);
		news.setLongitude(47.11);
		news.setRadius(10);
		NewsLocalization localization = new NewsLocalization();
		localization.setNews(news);
		localization.setId(1);
		localization.setLanguage("en");
		localization.setHeading("test");
		localization.setBody("this is a test message");
		news.addLocalization(localization);

		Collection<News> newsCollection = Lists.newArrayList(news);
		String result = NewsJsonGenerator.generateJsonAsStringForNews(newsCollection, "en");
		assertEquals(expectedResult, result);
	}

}
