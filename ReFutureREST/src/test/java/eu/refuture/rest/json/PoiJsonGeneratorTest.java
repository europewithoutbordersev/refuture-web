package eu.refuture.rest.json;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

import com.google.common.collect.Lists;

import eu.refuture.model.poi.PointOfInterest;
import eu.refuture.model.poi.PointOfInterestLocalization;
import eu.refuture.model.poi.PointOfInterestType;
import eu.refuture.model.poi.PointOfInterestTypeLocalization;

/**
 * @author Europe Without Borders e.V.
 * @version 1.0
 */
public class PoiJsonGeneratorTest {

	@Test
	public void testGenerateJsonForPoiWithoutRatio() {
		String expectedResult = "{\"id\":1,\"latitude\":13.37,\"longitude\":47.11,\"localizations\":[{\"id\":1,\"language\":\"de\",\"name\":\"Test Name\",\"description\":\"Test Beschreibung\",\"capacity\":\"100 Einheiten\"},{\"id\":2,\"language\":\"en\",\"name\":\"test name\",\"description\":\"test description\"}],\"type\":\"test\",\"type_localizations\":[{\"language\":\"de\",\"name\":\"Test Typ\",\"description\":\"Dies ist ein Typ zum testen\"},{\"language\":\"en\",\"name\":\"test type\",\"description\":\"this is a type for testing\"}]}";
		PointOfInterest poi = generatePointOfInterest();
		poi.setRatio(-1);
		
		String result = PoiJsonGenerator.generateJsonAsStringForPoi(poi, "en", "de");
		assertEquals(expectedResult, result);
	}
	
	@Test
	public void testGenerateJsonForPoiWithRatio() {
		String expectedResult = "{\"id\":1,\"latitude\":13.37,\"longitude\":47.11,\"ratio\":47,\"localizations\":[{\"id\":1,\"language\":\"de\",\"name\":\"Test Name\",\"description\":\"Test Beschreibung\",\"capacity\":\"100 Einheiten\"},{\"id\":2,\"language\":\"en\",\"name\":\"test name\",\"description\":\"test description\"}],\"type\":\"test\",\"type_localizations\":[{\"language\":\"de\",\"name\":\"Test Typ\",\"description\":\"Dies ist ein Typ zum testen\"},{\"language\":\"en\",\"name\":\"test type\",\"description\":\"this is a type for testing\"}]}";
		PointOfInterest poi = generatePointOfInterest();
		poi.setRatio(47);
		
		String result = PoiJsonGenerator.generateJsonAsStringForPoi(poi, "de", "en");
		assertEquals(expectedResult, result);
	}
	
	@Test
	public void testGenerateJsonAsStringForPois(){
		String expectedResult = "{\"locations\":[{\"id\":1,\"latitude\":13.37,\"longitude\":47.11,\"ratio\":47,\"localizations\":[{\"id\":1,\"language\":\"de\",\"name\":\"Test Name\",\"description\":\"Test Beschreibung\",\"capacity\":\"100 Einheiten\"}],\"type\":\"test\",\"type_localizations\":[{\"language\":\"de\",\"name\":\"Test Typ\",\"description\":\"Dies ist ein Typ zum testen\"}]}]}";
		
		PointOfInterest poi = generatePointOfInterest();
		poi.setRatio(47);
		
		List<PointOfInterest> pois = Lists.newArrayList(poi);
		
		String result = PoiJsonGenerator.generateJsonAsStringForPois(pois, "de");
		assertEquals(expectedResult, result);
	}
	
	private PointOfInterest generatePointOfInterest(){
		PointOfInterest poi = new PointOfInterest();
		poi.setId(1);
		poi.setLatitude(13.37);
		poi.setLongitude(47.11);
		
		PointOfInterestLocalization localizationDe = new PointOfInterestLocalization();
		localizationDe.setId(1);
		localizationDe.setLanguage("de");
		localizationDe.setDescription("Test Beschreibung");
		localizationDe.setName("Test Name");
		localizationDe.setCapacity("100 Einheiten");
		localizationDe.setPointOfInterest(poi);
		poi.addLocalization(localizationDe);
		
		PointOfInterestLocalization localizationEn = new PointOfInterestLocalization();
		localizationEn.setId(2);
		localizationEn.setLanguage("en");
		localizationEn.setDescription("test description");
		localizationEn.setName("test name");
		localizationEn.setPointOfInterest(poi);
		poi.addLocalization(localizationEn);
		
		PointOfInterestType poiType = new PointOfInterestType();
		poiType.setId("test");
		poi.setType(poiType);
		
		PointOfInterestTypeLocalization typeLocalizationDe = new PointOfInterestTypeLocalization();
		typeLocalizationDe.setLang("de");
		typeLocalizationDe.setName("Test Typ");
		typeLocalizationDe.setDescription("Dies ist ein Typ zum testen");
		typeLocalizationDe.setType(poiType);
		poiType.addLocalization(typeLocalizationDe);
		
		PointOfInterestTypeLocalization typeLocalizationEn = new PointOfInterestTypeLocalization();
		typeLocalizationEn.setLang("en");
		typeLocalizationEn.setName("test type");
		typeLocalizationEn.setDescription("this is a type for testing");
		typeLocalizationEn.setType(poiType);
		poiType.addLocalization(typeLocalizationEn);
		
		return poi;
	}

}
