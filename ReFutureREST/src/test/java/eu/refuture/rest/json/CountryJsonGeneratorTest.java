package eu.refuture.rest.json;

import static org.junit.Assert.*;

import java.util.Collection;

import org.junit.Test;

import com.google.common.collect.Lists;

import eu.refuture.model.country.Country;
import eu.refuture.model.country.CountryLocalization;

public class CountryJsonGeneratorTest {

	/**
	 * @author Europe Without Borders e.V.
	 * @version 1.0
	 */
	@Test
	public void testGetJsonAsStringForCountries() {
		String expectedResult = "{\"countries\":[{\"country_code\":\"DE\",\"localizations\":[{\"lang\":\"de\",\"name\":\"Deutschland\"},{\"lang\":\"en\",\"name\":\"Germany\"}]},{\"country_code\":\"DE\",\"localizations\":[{\"lang\":\"de\",\"name\":\"Frankreich\"},{\"lang\":\"en\",\"name\":\"France\"}]}]}";

		// Germany
		Country country1 = new Country();
		country1.setId("DE");

		CountryLocalization country1LocalizationDe = new CountryLocalization();
		country1LocalizationDe.setLang("de");
		country1LocalizationDe.setName("Deutschland");
		country1LocalizationDe.setCountry(country1);
		country1.addLocalization(country1LocalizationDe);

		CountryLocalization country1LocalizationEn = new CountryLocalization();
		country1LocalizationEn.setLang("en");
		country1LocalizationEn.setName("Germany");
		country1LocalizationEn.setCountry(country1);
		country1.addLocalization(country1LocalizationEn);

		// France
		Country country2 = new Country();
		country2.setId("DE");

		CountryLocalization country2LocalizationDe = new CountryLocalization();
		country2LocalizationDe.setLang("de");
		country2LocalizationDe.setName("Frankreich");
		country2LocalizationDe.setCountry(country2);
		country2.addLocalization(country2LocalizationDe);

		CountryLocalization country2LocalizationEn = new CountryLocalization();
		country2LocalizationEn.setLang("en");
		country2LocalizationEn.setName("France");
		country2LocalizationEn.setCountry(country2);
		country2.addLocalization(country2LocalizationEn);

		Collection<Country> countries = Lists.newArrayList(country1, country2);

		String result = CountryJsonGenerator.getJsonAsStringForCountries(countries);
		assertEquals(expectedResult, result);
	}

}
