package eu.refuture.dbinit;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.eclipse.persistence.sessions.server.ServerSession;
import org.eclipse.persistence.tools.schemaframework.SchemaManager;
import org.jasypt.digest.StandardStringDigester;

import eu.refuture.model.country.Country;
import eu.refuture.model.generators.CountriesGenerator;
import eu.refuture.model.generators.NewsGenerator;
import eu.refuture.model.generators.PoiGenerator;
import eu.refuture.model.generators.PoiTypeGenerator;
import eu.refuture.model.generators.UsersGenerator;
import eu.refuture.model.news.News;
import eu.refuture.model.poi.PointOfInterest;
import eu.refuture.model.poi.PointOfInterestType;
import eu.refuture.model.refugee.Refugee;
import eu.refuture.model.user.User;

/**
 * Creates the ReFuture Database. Depending on the arguments passed to the main
 * method it will create sample values.
 * 
 * @author Europe Without Borders e.V.
 * @version 1.0
 */
public class DbInit {

	private static final String PERSISTENCE_UNIT_NAME = "ReFuture";

	private static final String DEFAULT_HOST = "localhost";
	private static final String DEFAULT_PORT = "3306";
	private static final String DEFAULT_USER = "root";
	private static final String DEFAULT_PASSWORD = "";

	public static void main(String[] args) {
		String host = DEFAULT_HOST;
		String port = DEFAULT_PORT;
		String dbUser = DEFAULT_USER;
		String password = DEFAULT_PASSWORD;

		if (args.length >= 4) {
			host = args[0];
			port = args[1];
			dbUser = args[2];
			password = args[3];
		}

		boolean createSampleData = false;
		if (args.length == 1) {
			if ("createsampledata".equals(args[0])) {
				createSampleData = true;
			}
		}
		if (args.length == 5) {
			if ("createsampledata".equals(args[4])) {
				createSampleData = true;
			}
		}

		Map<String, String> properties = new HashMap<String, String>();
		properties.put("javax.persistence.jdbc.driver", "com.mysql.jdbc.Driver");
		properties.put("javax.persistence.jdbc.url", "jdbc:mysql://" + host + ":" + port + "/ReFuture");
		properties.put("javax.persistence.jdbc.user", dbUser);
		properties.put("javax.persistence.jdbc.password", password);
		EntityManagerFactory factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME, properties);
		EntityManager em = factory.createEntityManager();
		ServerSession session = em.unwrap(ServerSession.class);
		SchemaManager schemaManager = new SchemaManager(session);
		schemaManager.replaceDefaultTables(true, true);
		em.getTransaction().begin();

		// create a default user
		List<User> defaultUsers = UsersGenerator.getDefaultUsers();
		StandardStringDigester digester = new StandardStringDigester();
		for (User user : defaultUsers) {
			user.setPassword(digester.digest(user.getPassword()));
			em.persist(user);
		}

		// init countries
		Collection<Country> defaultCountries = CountriesGenerator.getDefaultCountries();
		for (Country country : defaultCountries) {
			em.persist(country);
		}

		if (createSampleData) {
			// create news
			List<News> defaultNews = NewsGenerator.getSampleNews();
			for (News news : defaultNews) {
				news.setCreatedBy(defaultUsers.get(0));
				em.persist(news);
			}
		}

		// create poi types
		Collection<PointOfInterestType> defaultTypes = PoiTypeGenerator.getDefaultPoiTypes();
		for (PointOfInterestType poiType : defaultTypes) {
			em.persist(poiType);
		}

		if (createSampleData) {
			List<PointOfInterest> pois = PoiGenerator.getSamplePois(defaultTypes);
			for (PointOfInterest poi : pois) {
				em.persist(poi);
			}
		}

		if (createSampleData) {
			// refugee "1" for testing
			em.persist(new Refugee());
		}

		em.getTransaction().commit();
		em.close();

	}

}
