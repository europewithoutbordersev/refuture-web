package eu.refuture.dbaccess.gcm;

import static org.junit.Assert.*;

import org.junit.Test;

import eu.refuture.model.news.NewsLocalization;

/**
 * @author Europe Without Borders e.V.
 * @version 1.0
 */
public class GcmNewsJsonGeneratorTest {

	@Test
	public void testGenerateGcmJsonForNews() {
		String expectedResult = "{\"to\":\"/topics/news_en\",\"data\":{\"id\":1}}";
		String topicRoot = "/topics/news_";
		long newsId = 1;
		NewsLocalization newsLocalization = new NewsLocalization();
		newsLocalization.setLanguage("en");
		String generatedResult = GcmNewsJsonGenerator.generateGcmJsonForNews(topicRoot, newsId, newsLocalization);
		assertNotNull(generatedResult);
		assertTrue(expectedResult.equals(generatedResult));
	}

}
