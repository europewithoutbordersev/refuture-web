package eu.refuture.dbaccess.gcm;

import static org.junit.Assert.*;

import org.junit.Test;

import eu.refuture.model.poi.PointOfInterest;

/**
 * @author Europe Without Borders e.V.
 * @version 1.0
 */
public class GcmPoiJsonGeneratorTest {

	@Test
	public void testGenerateJson() {
		String expectedResult = "{\"to\":\"locations\",\"data\":{\"id\":1}}";

		PointOfInterest poi = new PointOfInterest();
		poi.setId(1);
		poi.setLatitude(1.337);
		poi.setLongitude(47.11);
		String topic = "locations";

		String result = GcmPoiJsonGenerator.generateJson(topic, poi);
		assertEquals(expectedResult, result);
	}

}
