package eu.refuture.dbaccess.gcm;

import javax.json.Json;
import javax.json.JsonObjectBuilder;

import com.google.common.base.Preconditions;

import eu.refuture.model.news.NewsLocalization;

/**
 * Creates the JSON which will be sent via GCM if a new news has been created.
 * 
 * @author Europe Without Borders e.V.
 * @version 1.0
 */
public class GcmNewsJsonGenerator {

	public static String generateGcmJsonForNews(String topicRoot, long newsId, NewsLocalization newsLocalization) {
		// {"news_id": 42}
		Preconditions.checkNotNull(topicRoot);
		Preconditions.checkNotNull(newsLocalization);
		Preconditions.checkNotNull(newsLocalization.getLanguage());

		JsonObjectBuilder dataObjectBuilder = Json.createObjectBuilder().add("id", newsId);

		JsonObjectBuilder gcmObjectBuilder = Json.createObjectBuilder();
		gcmObjectBuilder.add("to", topicRoot + newsLocalization.getLanguage());
		gcmObjectBuilder.add("data", dataObjectBuilder);

		return gcmObjectBuilder.build().toString();
	}
}
