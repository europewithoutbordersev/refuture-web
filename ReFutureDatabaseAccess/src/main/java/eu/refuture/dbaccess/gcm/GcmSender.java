package eu.refuture.dbaccess.gcm;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import eu.refuture.model.news.News;
import eu.refuture.model.news.NewsLocalization;
import eu.refuture.model.poi.PointOfInterest;

/**
 * Class for sending events via Google Cloud Messaging if new Pois or news have
 * been created by a webportal-user
 * 
 * @author Europe Without Borders e.V.
 * @version 1.0
 */
public class GcmSender {

	private static final int HTTP_CLIENT_TIMEOUT = 30; // seconds

	private CloseableHttpClient httpClient;

	private String gcmUrl;
	private String gcmKey;

	public GcmSender(String gcmUrl, String gcmKey) {
		this.gcmUrl = gcmUrl;
		this.gcmKey = gcmKey;

		RequestConfig config = RequestConfig.custom().setSocketTimeout(HTTP_CLIENT_TIMEOUT * 1000)
				.setConnectTimeout(HTTP_CLIENT_TIMEOUT * 1000).build();
		httpClient = HttpClients.custom().setDefaultRequestConfig(config).build();
	}

	public void sendNewsNotification(News news) {

		if (gcmUrl != null && news != null && news.getLocalizations() != null) {

			for (NewsLocalization localization : news.getLocalizations()) {
				String requestJson = "";
				requestJson = GcmNewsJsonGenerator.generateGcmJsonForNews("/topics/news_", news.getId(), localization);

				sendPostRequest(requestJson);

			}
		}
	}

	public void sendPoiNotification(PointOfInterest poi) {
		if (gcmUrl != null && poi != null) {
			String requestJson = GcmPoiJsonGenerator.generateJson("/topics/locations", poi);

			sendPostRequest(requestJson);
		}
	}

	private void sendPostRequest(String json) {
		CloseableHttpResponse response = null;
		try {
			HttpPost httpPost = new HttpPost(gcmUrl);
			httpPost.addHeader("Content-Type", "application/json");
			httpPost.addHeader("Authorization", "key=" + gcmKey);
			httpPost.setEntity(new StringEntity(json));
			response = httpClient.execute(httpPost);
			System.out.println(
					response.getStatusLine().getStatusCode() + " - " + response.getStatusLine().getReasonPhrase());
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (response != null) {
				try {
					response.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
