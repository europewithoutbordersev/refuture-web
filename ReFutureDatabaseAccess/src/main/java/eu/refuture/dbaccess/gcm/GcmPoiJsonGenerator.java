package eu.refuture.dbaccess.gcm;

import javax.json.Json;
import javax.json.JsonObjectBuilder;

import com.google.common.base.Preconditions;

import eu.refuture.model.poi.PointOfInterest;

/**
 * Creates the JSON which will be sent via Google Cloud Message if a new POI has
 * been created.
 * 
 * @author Europe Without Borders e.V.
 * @version 1.0
 */
public class GcmPoiJsonGenerator {
	public static String generateJson(String topic, PointOfInterest poi) {
		Preconditions.checkNotNull(topic);
		Preconditions.checkNotNull(poi);

		JsonObjectBuilder dataObjectBuilder = Json.createObjectBuilder().add("id", poi.getId());

		JsonObjectBuilder gcmObjectBuilder = Json.createObjectBuilder();
		gcmObjectBuilder.add("to", topic);
		gcmObjectBuilder.add("data", dataObjectBuilder);

		return gcmObjectBuilder.build().toString();
	}
}
