package eu.refuture.dbaccess;

import java.util.Date;
import java.util.List;

import eu.refuture.model.country.Country;
import eu.refuture.model.news.News;
import eu.refuture.model.poi.PointOfInterest;
import eu.refuture.model.poi.PointOfInterestType;
import eu.refuture.model.refugee.Refugee;
import eu.refuture.model.user.User;

/**
 * API of the data-access.
 * 
 * @author Europe Without Borders e.V.
 * @version 1.0
 */
public interface IDatabaseAccess {
	User getUserByMail(String mail);

	void saveUser(User user);

	boolean authenticateUser(String mail, String password);

	void saveNews(News news);

	News getNewsById(long newsId);

	News getNewsByIdAndLanguage(long newsId, String language);

	News getNewsByIdAndLanguages(long newsId, String... languages);

	List<News> getNews(Date date, String... languages);

	List<Country> getCountries();

	void savePoi(PointOfInterest poi);

	List<PointOfInterestType> getPoiTypes();

	PointOfInterest getPoiById(long id);

	List<PointOfInterest> getPois(Date date);

	Country getCountryById(String startCountryCode);

	long saveNewRefugee(Refugee refugee);

	void updateRefugee(Refugee refugee);

	void savePosition(long refugeeId, double latitude, double longitude);

	boolean validateRefugeeId(long refugeeId);
}
