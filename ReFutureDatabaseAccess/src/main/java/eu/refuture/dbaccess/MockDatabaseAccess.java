package eu.refuture.dbaccess;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.common.collect.Lists;

import eu.refuture.model.country.Country;
import eu.refuture.model.generators.CountriesGenerator;
import eu.refuture.model.generators.PoiTypeGenerator;
import eu.refuture.model.generators.UsersGenerator;
import eu.refuture.model.news.News;
import eu.refuture.model.poi.PointOfInterest;
import eu.refuture.model.poi.PointOfInterestType;
import eu.refuture.model.refugee.Refugee;
import eu.refuture.model.user.User;

/**
 * Mock-Implementation of some of the dataacess-methods. Can be used for testing
 * purposes :)
 * 
 * @author Europe Without Borders e.V.
 * @version 1.0
 */
public class MockDatabaseAccess implements IDatabaseAccess {

	private Map<String, User> mockUsers = new HashMap<String, User>();
	private Map<Long, News> mockNews = new HashMap<Long, News>();
	private Map<String, Country> mockCountries = new HashMap<String, Country>();
	private Map<Long, PointOfInterest> mockPois = new HashMap<Long, PointOfInterest>();

	public MockDatabaseAccess() {
		Collection<User> defaultUsers = UsersGenerator.getDefaultUsers();
		for (User user : defaultUsers) {
			mockUsers.put(user.geteMail(), user);
		}

		Collection<Country> defaultCountries = CountriesGenerator.getDefaultCountries();
		for (Country country : defaultCountries) {
			mockCountries.put(country.getId(), country);
		}
	}

	@Override
	public User getUserByMail(String mail) {
		return mockUsers.get(mail);
	}

	@Override
	public void saveUser(User user) {
		mockUsers.put(user.geteMail(), user);
	}

	@Override
	public boolean authenticateUser(String mail, String password) {
		boolean authenticate = false;
		if (mail != null) {
			User user = mockUsers.get(mail);
			if (user != null && user.getPassword() != null && user.getPassword().equals(password)) {
				authenticate = true;
			}
		}
		return authenticate;

	}

	@Override
	public void saveNews(News news) {
		news.setId(mockNews.size() + 1);
		mockNews.put(news.getId(), news);
		// new GcmSender().sendNewsNotification(news);
	}

	@Override
	public News getNewsById(long newsId) {
		return mockNews.get(newsId);
	}

	@Override
	public News getNewsByIdAndLanguage(long newsId, String language) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Country> getCountries() {
		List<Country> countries = Lists.newArrayList();
		countries.addAll(mockCountries.values());
		return countries;
	}

	@Override
	public void savePoi(PointOfInterest poi) {
		poi.setId(mockPois.size() + 1);
		mockPois.put(poi.getId(), poi);
	}

	@Override
	public List<PointOfInterestType> getPoiTypes() {
		return PoiTypeGenerator.getDefaultPoiTypes();
	}

	@Override
	public PointOfInterest getPoiById(long id) {
		return mockPois.get(id);
	}

	@Override
	public News getNewsByIdAndLanguages(long newsId, String... languages) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<News> getNews(Date date, String... languages) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<PointOfInterest> getPois(Date date) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Country getCountryById(String startCountryCode) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long saveNewRefugee(Refugee refugee) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void updateRefugee(Refugee refugee) {
		// TODO Auto-generated method stub

	}

	@Override
	public void savePosition(long refugeeId, double latitude, double longitude) {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean validateRefugeeId(long refugeeId) {
		// TODO Auto-generated method stub
		return false;
	}

}
