package eu.refuture.dbaccess;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.jasypt.digest.StandardStringDigester;

import com.google.common.collect.Lists;
import com.netflix.config.DynamicPropertyFactory;
import com.netflix.config.DynamicStringProperty;

import eu.refuture.dbaccess.gcm.GcmSender;
import eu.refuture.model.country.Country;
import eu.refuture.model.news.News;
import eu.refuture.model.news.NewsLocalization;
import eu.refuture.model.poi.PointOfInterest;
import eu.refuture.model.poi.PointOfInterestType;
import eu.refuture.model.refugee.Refugee;
import eu.refuture.model.refugee.RefugeePosition;
import eu.refuture.model.user.User;

/**
 * Class to access the mysql-database
 * 
 * @author Europe Without Borders e.V.
 * @version 1.0
 */
public class DatabaseAccess implements IDatabaseAccess {

	private static final String PERSISTENCE_UNIT_NAME = "ReFuture";
	private static EntityManagerFactory factory;
	private GcmSender gcmSender;

	public DatabaseAccess() {
		Map<String, String> properties = new HashMap<String, String>();
		DynamicStringProperty dbUserProp = DynamicPropertyFactory.getInstance().getStringProperty("database.user",
				"root");
		DynamicStringProperty dbPasswordProp = DynamicPropertyFactory.getInstance()
				.getStringProperty("database.password", "");
		properties.put("javax.persistence.jdbc.driver", "com.mysql.jdbc.Driver");
		properties.put("javax.persistence.jdbc.url", "jdbc:mysql://localhost:3306/ReFuture");
		properties.put("javax.persistence.jdbc.user", dbUserProp.get());
		properties.put("javax.persistence.jdbc.password", dbPasswordProp.get());
		factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME, properties);

		DynamicStringProperty targetUrlProperty = DynamicPropertyFactory.getInstance().getStringProperty("gcm.url",
				"https://gcm-http.googleapis.com/gcm/send");
		DynamicStringProperty gcmKeyProperty = DynamicPropertyFactory.getInstance()
				.getStringProperty("gcm.authorization.key", "AIzaSyDFNhIXFqmhWrYtG-5CAfJi0fwBs8Pyb_I");
		gcmSender = new GcmSender(targetUrlProperty.get(), gcmKeyProperty.get());
	}

	public User getUserByMail(String mail) {
		EntityManager em = factory.createEntityManager();
		em.getEntityManagerFactory().getCache().evictAll(); // TODO necessary?
		CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
		CriteriaQuery<User> criteriaQuery = criteriaBuilder.createQuery(User.class);
		Root<User> userRoot = criteriaQuery.from(User.class);
		criteriaQuery.select(userRoot).where(criteriaBuilder.equal(userRoot.get("eMail"), mail));
		Query query = em.createQuery(criteriaQuery);
		List<User> foundUsers = (List<User>) query.getResultList();
		em.close();
		if (foundUsers.isEmpty()) {
			return null;
		} else {
			return foundUsers.get(0);
		}
	}

	/**
	 * @throws IllegalArgumentException
	 *             if mail is already taken or mail is invalid
	 */
	@Override
	public void saveUser(User user) {
		// TODO Auto-generated method stub
		if (checkIfMailIsAlreadyInUse(user.geteMail())) {
			throw new IllegalArgumentException("mail");
		} else {
			// Hash password
			StandardStringDigester digester = new StandardStringDigester();
			// digester.setAlgorithm("SHA-1"); // optionally set the algorithm
			// digester.setIterations(50000); // increase security by
			// performing 50000 hashing iterations
			user.setPassword(digester.digest(user.getPassword()));
			System.out.println("Password-Hash: " + user.getPassword());

			EntityManager em = factory.createEntityManager();
			em.getTransaction().begin();
			em.persist(user);
			em.getTransaction().commit();
			em.close();
		}
	}

	private boolean checkIfMailIsAlreadyInUse(String mail) {
		boolean isInUse = true;
		if (getUserByMail(mail) == null) {
			isInUse = false;
		}
		return isInUse;
	}

	public boolean authenticateUser(String mail, String password) {
		User user = getUserByMail(mail);
		if (user == null) {
			return false;
		} else {
			StandardStringDigester digester = new StandardStringDigester();
			return digester.matches(password, user.getPassword());
		}
	}

	void setEntityManagerFactory(EntityManagerFactory factory) {
		this.factory = factory;
	}

	@Override
	public void saveNews(News news) {
		EntityManager em = factory.createEntityManager();
		em.getTransaction().begin();
		em.persist(news);
		em.getTransaction().commit();
		gcmSender.sendNewsNotification(news);
		em.close();
	}

	@Override
	public News getNewsById(long newsId) {
		EntityManager em = factory.createEntityManager();
		// em.getEntityManagerFactory().getCache().evictAll(); // TODO
		// necessary?
		CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
		CriteriaQuery<News> criteriaQuery = criteriaBuilder.createQuery(News.class);
		Root<News> newsRoot = criteriaQuery.from(News.class);
		criteriaQuery.select(newsRoot).where(criteriaBuilder.equal(newsRoot.get("id"), newsId));
		Query query = em.createQuery(criteriaQuery);
		List<News> foundNews = (List<News>) query.getResultList();
		em.close();
		if (foundNews.isEmpty()) {
			return null;
		} else {
			return foundNews.get(0);
		}
	}

	@Override
	public List<Country> getCountries() {
		EntityManager em = factory.createEntityManager();
		// em.getEntityManagerFactory().getCache().evictAll(); // TODO
		// necessary?
		CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
		CriteriaQuery<Country> criteriaQuery = criteriaBuilder.createQuery(Country.class);
		Query query = em.createQuery(criteriaQuery);
		List<Country> countries = (List<Country>) query.getResultList();
		return countries;
	}

	@Override
	public void savePoi(PointOfInterest poi) {
		EntityManager em = factory.createEntityManager();
		em.getTransaction().begin();
		em.persist(poi);
		em.getTransaction().commit();
		gcmSender.sendPoiNotification(poi);
		em.close();
		System.out.println(poi.getCreatedAt().getTime());
	}

	@Override
	public List<PointOfInterestType> getPoiTypes() {
		EntityManager em = factory.createEntityManager();
		// em.getEntityManagerFactory().getCache().evictAll(); // TODO
		// necessary?
		CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
		CriteriaQuery<PointOfInterestType> criteriaQuery = criteriaBuilder.createQuery(PointOfInterestType.class);
		Query query = em.createQuery(criteriaQuery);
		List<PointOfInterestType> poiTypes = (List<PointOfInterestType>) query.getResultList();
		return poiTypes;
	}

	@Override
	public PointOfInterest getPoiById(long id) {
		EntityManager em = factory.createEntityManager();
		// em.getEntityManagerFactory().getCache().evictAll(); // TODO
		// necessary?
		CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
		CriteriaQuery<PointOfInterest> criteriaQuery = criteriaBuilder.createQuery(PointOfInterest.class);
		Root<PointOfInterest> poiRoot = criteriaQuery.from(PointOfInterest.class);
		criteriaQuery.select(poiRoot).where(criteriaBuilder.equal(poiRoot.get("id"), id));
		Query query = em.createQuery(criteriaQuery);
		List<PointOfInterest> foundPois = (List<PointOfInterest>) query.getResultList();
		em.close();
		if (foundPois.isEmpty()) {
			return null;
		} else {
			return foundPois.get(0);
		}
	}

	@Override
	public List<PointOfInterest> getPois(Date date) {
		EntityManager em = factory.createEntityManager();
		// em.getEntityManagerFactory().getCache().evictAll(); // TODO
		// necessary?
		CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
		CriteriaQuery<PointOfInterest> criteriaQuery = criteriaBuilder.createQuery(PointOfInterest.class);
		Root<PointOfInterest> poiRoot = criteriaQuery.from(PointOfInterest.class);

		if (date != null) {
			Predicate datePredicate = criteriaBuilder.greaterThanOrEqualTo(poiRoot.<Date> get("createdAt"), date);
			criteriaQuery.select(poiRoot).where(datePredicate);
		}

		Query query = em.createQuery(criteriaQuery);

		List<PointOfInterest> foundPois = (List<PointOfInterest>) query.getResultList();
		em.close();
		if (foundPois.isEmpty()) {
			return null;
		} else {
			return foundPois;
		}
	}

	@Override
	public News getNewsByIdAndLanguages(long newsId, String... languages) {
		EntityManager em = factory.createEntityManager();
		CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
		CriteriaQuery<News> criteriaQuery = criteriaBuilder.createQuery(News.class);
		Root<News> news = criteriaQuery.from(News.class);
		Root<NewsLocalization> localization = criteriaQuery.from(NewsLocalization.class);
		Join<News, NewsLocalization> join = news.join("localizations");

		List<Predicate> languagePredicates = Lists.newArrayList();
		for (String language : languages) {
			languagePredicates.add(criteriaBuilder.equal(join.get("language"), language));
		}
		Predicate languagesPredicate = criteriaBuilder
				.or(languagePredicates.toArray(new Predicate[languagePredicates.size()]));
		Predicate idPredicate = criteriaBuilder.equal(news.get("id"), newsId);
		Predicate completePredicate = criteriaBuilder.and(idPredicate, languagesPredicate);
		criteriaQuery = criteriaQuery.select(news).where(completePredicate).distinct(true);
		// criteriaQuery = criteriaQuery.select(news).where(idPredicate);

		Query query = em.createQuery(criteriaQuery);
		List<News> foundNews = (List<News>) query.getResultList();
		em.close();
		if (foundNews.isEmpty()) {
			return null;
		} else {
			foundNews.get(0).getLocalizations();
			return foundNews.get(0);
		}
	}

	@Override
	public List<News> getNews(Date date, String... languages) {
		EntityManager em = factory.createEntityManager();
		CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
		CriteriaQuery<News> criteriaQuery = criteriaBuilder.createQuery(News.class);
		Root<News> news = criteriaQuery.from(News.class);
		Root<NewsLocalization> localization = criteriaQuery.from(NewsLocalization.class);
		Join<News, NewsLocalization> join = news.join("localizations");

		List<Predicate> languagePredicates = Lists.newArrayList();
		for (String language : languages) {
			languagePredicates.add(criteriaBuilder.equal(join.get("language"), language));
		}
		Predicate languagesPredicate = criteriaBuilder
				.or(languagePredicates.toArray(new Predicate[languagePredicates.size()]));

		Predicate finalPredicate;
		if (date == null) {
			finalPredicate = languagesPredicate;
		} else {
			Predicate datePredicate = criteriaBuilder.greaterThanOrEqualTo(news.<Date> get("createdAt"), date);
			finalPredicate = criteriaBuilder.and(languagesPredicate, datePredicate);
		}
		criteriaQuery = criteriaQuery.select(news).where(finalPredicate).distinct(true);

		Query query = em.createQuery(criteriaQuery);
		List<News> foundNews = (List<News>) query.getResultList();
		em.close();
		return foundNews;
	}

	@Override
	public News getNewsByIdAndLanguage(long newsId, String language) {
		EntityManager em = factory.createEntityManager();
		// em.getEntityManagerFactory().getCache().evictAll(); // TODO
		// necessary?
		CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
		CriteriaQuery<News> criteriaQuery = criteriaBuilder.createQuery(News.class);
		Root<News> news = criteriaQuery.from(News.class);
		Root<NewsLocalization> localization = criteriaQuery.from(NewsLocalization.class);
		Join<News, NewsLocalization> join = news.join("localizations");

		Predicate p1 = criteriaBuilder.equal(join.get("language"), language);
		Predicate p2 = criteriaBuilder.equal(news.get("id"), newsId);
		Predicate p3 = criteriaBuilder.and(p1, p2);
		criteriaQuery.select(news).where(p3);

		Query query = em.createQuery(criteriaQuery);
		List<News> foundNews = (List<News>) query.getResultList();
		em.close();
		if (foundNews.isEmpty()) {
			return null;
		} else {
			return foundNews.get(0);
		}
	}

	@Override
	public Country getCountryById(String startCountryCode) {
		EntityManager em = factory.createEntityManager();
		// em.getEntityManagerFactory().getCache().evictAll(); // TODO
		// necessary?
		CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
		CriteriaQuery<Country> criteriaQuery = criteriaBuilder.createQuery(Country.class);
		Root<Country> countryRoot = criteriaQuery.from(Country.class);
		criteriaQuery.select(countryRoot).where(criteriaBuilder.equal(countryRoot.get("id"), startCountryCode));
		Query query = em.createQuery(criteriaQuery);
		List<Country> countries = (List<Country>) query.getResultList();
		em.close();
		if (countries.isEmpty()) {
			return null;
		} else {
			return countries.get(0);
		}
	}

	@Override
	public long saveNewRefugee(Refugee refugee) {
		EntityManager em = factory.createEntityManager();
		em.getTransaction().begin();
		em.persist(refugee);
		em.getTransaction().commit();
		em.close();
		return refugee.getId();
	}

	@Override
	public void updateRefugee(Refugee refugee) {
		EntityManager em = factory.createEntityManager();
		em.getTransaction().begin();
		em.merge(refugee);
		em.getTransaction().commit();
		em.close();
	}

	private Refugee getRefugeeById(long refugeeId) {
		EntityManager em = factory.createEntityManager();
		// em.getEntityManagerFactory().getCache().evictAll(); // TODO
		// necessary?
		CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
		CriteriaQuery<Refugee> criteriaQuery = criteriaBuilder.createQuery(Refugee.class);
		Root<Refugee> refugeeRoot = criteriaQuery.from(Refugee.class);
		criteriaQuery.select(refugeeRoot).where(criteriaBuilder.equal(refugeeRoot.get("id"), refugeeId));
		Query query = em.createQuery(criteriaQuery);
		List<Refugee> refugees = (List<Refugee>) query.getResultList();
		em.close();
		if (refugees.isEmpty()) {
			return null;
		} else {
			return refugees.get(0);
		}
	}

	@Override
	public void savePosition(long refugeeId, double latitude, double longitude) {
		Refugee refugee = getRefugeeById(refugeeId);
		if (refugee != null) {
			RefugeePosition position = new RefugeePosition();
			position.setRefugee(refugee);
			position.setLatitude(latitude);
			position.setLongitude(longitude);
			position.setTimestamp(new Date());
			refugee.addPosition(position);

			EntityManager em = factory.createEntityManager();
			em.getTransaction().begin();
			em.persist(position);
			em.merge(refugee);
			em.getTransaction().commit();
			em.close();
		}
	}

	@Override
	public boolean validateRefugeeId(long refugeeId) {
		if (getRefugeeById(refugeeId) == null) {
			return false;
		} else {
			return true;
		}
	}
}
