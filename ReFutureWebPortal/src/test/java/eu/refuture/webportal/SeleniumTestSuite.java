package eu.refuture.webportal;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import eu.refuture.webportal.selenium.ReFuturePortalTest;

@RunWith(Suite.class)
@SuiteClasses({ ReFuturePortalTest.class })
public class SeleniumTestSuite {

}
