package eu.refuture.webportal.selenium;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;

import com.google.common.base.Function;

public class ReFuturePortalTest {
	private String baseUrl;
	private WebDriver driver;
	private ScreenshotHelper screenshotHelper;

	@Before
	public void openBrowser() {
		baseUrl = System.getProperty("webdriver.base.url");
		DesiredCapabilities caps = new DesiredCapabilities();
		caps.setJavascriptEnabled(true); // < not really needed: JS enabled by
		// default
		caps.setCapability("takesScreenshot", true); // < yeah, GhostDriver
		// haz screenshotz!
		// caps.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY,
		// System.getProperty("phantomjs.bin.path"));
		boolean useFirefox = Boolean.valueOf(System.getProperty("webdriver.use.firefox"));
		if (useFirefox) {
			driver = new FirefoxDriver();
		} else {
			driver = new PhantomJSDriver(caps);
			driver.manage().window().setSize(new Dimension(1680, 1050));
		}
		// driver.get(baseUrl);
		screenshotHelper = new ScreenshotHelper();
	}

	@After
	public void saveScreenshotAndCloseBrowser() throws IOException {
		screenshotHelper.saveScreenshot("screenshot");
		driver.quit();
	}

	@Test
	public void loginLogoutTest() throws IOException {
		driver.get(baseUrl);

		Wait<WebDriver> wait = new FluentWait<WebDriver>(driver).withTimeout(30, TimeUnit.SECONDS)
				.pollingEvery(5, TimeUnit.SECONDS).ignoring(NoSuchElementException.class);

		WebElement mailField = wait.until(new Function<WebDriver, WebElement>() {
			public WebElement apply(WebDriver driver) {
				return driver.findElement(By.id("loginFieldMail"));
			}
		});
		mailField.click();
		mailField.clear();
		mailField.sendKeys("admin@example.org");

		WebElement passwordField = wait.until(new Function<WebDriver, WebElement>() {
			public WebElement apply(WebDriver driver) {
				return driver.findElement(By.id("loginFieldPassword"));
			}
		});
		passwordField.click();
		passwordField.clear();
		passwordField.sendKeys("admin");

		WebElement loginButton = wait.until(new Function<WebDriver, WebElement>() {
			public WebElement apply(WebDriver driver) {
				return driver.findElement(By.id("loginButtonLogin"));
			}
		});
		loginButton.click();

		WebElement mainMenu = wait.until(new Function<WebDriver, WebElement>() {
			public WebElement apply(WebDriver driver) {
				return driver.findElement(By.id("mainMenuBar"));
			}
		});

		screenshotHelper.saveScreenshot("screenshot");

		WebElement userSubMenu = wait.until(new Function<WebDriver, WebElement>() {
			public WebElement apply(WebDriver driver) {
				return driver.findElement(By.cssSelector("span.v-menubar-menuitem"));
			}
		});
		screenshotHelper.saveScreenshot("screenshot2");
		userSubMenu.click();

		WebElement logoutMenuItem = wait.until(new Function<WebDriver, WebElement>() {
			public WebElement apply(WebDriver driver) {
				return driver.findElement(By.xpath("//span[contains(.,'Logout')]"));
			}
		});
		screenshotHelper.saveScreenshot("screenshot3");
		logoutMenuItem.click();

		passwordField = wait.until(new Function<WebDriver, WebElement>() {
			public WebElement apply(WebDriver driver) {
				return driver.findElement(By.id("loginFieldPassword"));
			}
		});

		// assertEquals("The page title should equal Google at the start of the
		// test.", "Google", driver.getTitle());
		// WebElement searchField = driver.findElement(By.name("q"));
		// searchField.sendKeys("Drupal!");
		// searchField.submit();
	}

	private class ScreenshotHelper {

		public void saveScreenshot(String screenshotFileName) throws IOException {
			File screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			File screenshotFile = new File(
					"target/seleniumscreenshots/" + screenshotFileName + " " + new Date().toString() + ".png");
			FileUtils.copyFile(screenshot, screenshotFile);
			System.out.println(screenshotFile.getAbsolutePath());
		}
	}
}
