package eu.refuture.webportal;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import eu.refuture.webportal.i18n.I18nTest;

@RunWith(Suite.class)
@SuiteClasses({ I18nTest.class })
public class UnitTestSuite {

}
