package eu.refuture.webportal.i18n;

import static org.junit.Assert.assertEquals;

import java.util.Locale;

import org.junit.Test;

public class I18nTest {

	@Test
	public void testGetMessage() {
		assertEquals("erfolgreich", I18n.getMessage("test.text.resource", new Locale("de")));
		assertEquals("successful", I18n.getMessage("test.text.resource2", new Locale("de")));
		assertEquals("test.text.resource3", I18n.getMessage("test.text.resource3", new Locale("de")));
	}

}
