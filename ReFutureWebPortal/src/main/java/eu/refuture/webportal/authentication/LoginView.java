package eu.refuture.webportal.authentication;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Strings;
import com.vaadin.event.FieldEvents.BlurEvent;
import com.vaadin.event.FieldEvents.BlurListener;
import com.vaadin.event.FieldEvents.FocusEvent;
import com.vaadin.event.FieldEvents.FocusListener;
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.Page;
import com.vaadin.server.UserError;
import com.vaadin.ui.AbstractTextField;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Component;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Panel;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

import eu.refuture.webportal.i18n.I18n;

/**
 * Simple Login form to authenticate users if they want to access the webportal
 * 
 * @author Europe Without Borders e.V.
 * @version 1.0
 */
@SuppressWarnings("serial")
public class LoginView extends VerticalLayout {

	private final static Logger LOGGER = LoggerFactory.getLogger(LoginView.class);

	private AccessControl accessControl;
	private LoginViewListener loginViewListener;

	private TextField mailField;
	private PasswordField passwordField;
	private Button loginButton;

	public LoginView(AccessControl accessControl, LoginViewListener loginViewListener) {
		super();
		LOGGER.debug("initalize new LoginView");

		this.accessControl = accessControl;
		this.loginViewListener = loginViewListener;

		// style configuration of verticalLayout
		setSpacing(true);
		setWidth("100%");
		setHeight("100%");

		// TODO some kind of logo?

		// build login form
		Component loginForm = buildLoginForm();

		// panel as wraper around login form
		Panel loginPanel = new Panel(I18n.getMessage("login.caption"));
		loginPanel.setSizeUndefined();
		loginPanel.setContent(loginForm);
		addComponent(loginPanel);
		setComponentAlignment(loginPanel, Alignment.MIDDLE_CENTER);
	}

	private Component buildLoginForm() {
		LOGGER.debug("build new LoginForm");

		// layout for login form
		VerticalLayout loginFormLayout = new VerticalLayout();
		loginFormLayout.setSpacing(true);
		loginFormLayout.setMargin(true);

		// mail input field
		mailField = new TextField(I18n.getMessage("login.field.mail"));
		mailField.setId("loginFieldMail");
		addEnterKeyActionToTextField(mailField);
		mailField.setWidth("250px");
		loginFormLayout.addComponent(mailField);
		loginFormLayout.setComponentAlignment(mailField, Alignment.TOP_CENTER);

		// password input field
		passwordField = new PasswordField(I18n.getMessage("login.field.password"));
		passwordField.setId("loginFieldPassword");
		addEnterKeyActionToTextField(passwordField);
		passwordField.setWidth("250px");
		loginFormLayout.addComponent(passwordField);
		loginFormLayout.setComponentAlignment(passwordField, Alignment.TOP_CENTER);

		// login button
		loginButton = new Button(I18n.getMessage("login.button.login"));
		loginButton.setId("loginButtonLogin");
		loginButton.setIcon(FontAwesome.SIGN_IN);
		// add a button click listener for the login button
		loginButton.addClickListener(new ClickListener() {

			@Override
			public void buttonClick(ClickEvent event) {
				loginAttempt();
			}
		});
		loginFormLayout.addComponent(loginButton);
		loginFormLayout.setComponentAlignment(loginButton, Alignment.TOP_CENTER);

		return loginFormLayout;
	}

	private void loginAttempt() {
		// remove componentErrors if there are any
		mailField.setComponentError(null);
		passwordField.setComponentError(null);

		LOGGER.info("login-attempt with mail: " + mailField.getValue());

		// check if both input fields are filled
		if (Strings.isNullOrEmpty(mailField.getValue()) || Strings.isNullOrEmpty(passwordField.getValue())) {
			if (Strings.isNullOrEmpty(mailField.getValue())) {
				// mail field is empty
				mailField.setComponentError(new UserError("Field is empty")); // TODO
																				// I18n
			}
			if (Strings.isNullOrEmpty(passwordField.getValue())) {
				// password field is empty
				passwordField.setComponentError(new UserError("Field is empty"));
			}
		} else {
			// both fields are NOT empty
			if (accessControl.signIn(mailField.getValue().trim().toLowerCase(),
					passwordField.getValue().trim().toLowerCase())) {
				// login successful
				LOGGER.info("login successful with mail: " + mailField.getValue());
				loginViewListener.loginSuccessful();
			} else {
				showNotification(new Notification("Login fehlgeschlagen", // TODO
																			// I18n
						"Bitte überprüfen Sie die eingegebene Mail-Adresse und das Passwort.",
						Notification.Type.ERROR_MESSAGE));
				mailField.focus();
			}
		}
	}

	private void showNotification(Notification notification) {
		// keep the notification visible a little while after moving the
		// mouse, or until clicked
		notification.setDelayMsec(2000);
		notification.show(Page.getCurrent());
	}

	private void addEnterKeyActionToTextField(final AbstractTextField textField) {
		textField.addFocusListener(new FocusListener() {
			// If textfield is focused, add shortcut to search-button
			@Override
			public void focus(final FocusEvent event) {
				loginButton.setClickShortcut(KeyCode.ENTER);
			}
		});
		textField.addBlurListener(new BlurListener() {
			// if textfield loses focus, remove click-shortcut from
			// sparePartsSearch-Button
			@Override
			public void blur(final BlurEvent event) {
				loginButton.removeClickShortcut();
			}
		});
	}

	public interface LoginViewListener {
		void loginSuccessful();
	}
}
