package eu.refuture.webportal.authentication;

import eu.refuture.webportal.data.DataUtils;

/**
 * Very simple implementation of the Access Control * @author Europe Without
 * Borders e.V.
 * 
 * @version 1.0
 */
public class BasicAccessControl implements AccessControl {

	@Override
	public boolean signIn(String mail, String password) {
		if (mail == null || mail.isEmpty()) {
			return false;
		} else {
			if (DataUtils.getDatabaseUtility().authenticateUser(mail, password)) {
				CurrentUser.set(mail);
				return true;
			} else {
				return false;
			}
		}
	}

	@Override
	public boolean isUserSignedIn() {
		return !CurrentUser.get().isEmpty();
	}

	@Override
	public String getUserName() {
		return CurrentUser.get();
	}

}
