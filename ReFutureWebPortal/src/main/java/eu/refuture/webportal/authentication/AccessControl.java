package eu.refuture.webportal.authentication;

/**
 * Interface which defines an API for the AccessControl which will be used by
 * the Webportal
 * 
 * @author Bene * @author Europe Without Borders e.V.
 * @version 1.0
 */
public interface AccessControl {

	public boolean signIn(String username, String password);

	public boolean isUserSignedIn();

	public String getUserName();

}
