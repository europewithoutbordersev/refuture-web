package eu.refuture.webportal.administration.usermanagement;

import com.google.common.base.Strings;
import com.vaadin.server.UserError;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

import eu.refuture.model.user.User;
import eu.refuture.model.user.UserRoles;
import eu.refuture.webportal.i18n.I18n;

/**
 * A view to create new user accounts for the webportal
 * 
 * @author Europe Without Borders e.V.
 * @version 1.0
 */
@SuppressWarnings("serial")
public class CreateNewUserView extends VerticalLayout implements Button.ClickListener {

	private CreateNewUserViewListener listener;

	private TextField nameTextField;
	private TextField mailTextField;
	private PasswordField passwordField;
	private PasswordField confirmPasswordField;
	private TextArea postalAddressField;
	private ComboBox roleSelection;

	public CreateNewUserView(CreateNewUserViewListener componentListener) {
		super();
		this.listener = componentListener;
		setSpacing(true);
		setMargin(true);
		setWidth("100%");

		Label title = new Label(I18n.getMessage("admin.um.create.title"));
		title.addStyleName(ValoTheme.LABEL_H1);
		addComponent(title);

		FormLayout formLayout = new FormLayout();
		formLayout.setWidth("100%");
		addComponent(formLayout);

		nameTextField = new TextField(I18n.getMessage("admin.um.create.input.name"));
		nameTextField.setWidth("100%");
		nameTextField.setId("createUserNameField");
		formLayout.addComponent(nameTextField);

		mailTextField = new TextField(I18n.getMessage("admin.um.create.input.mail"));
		mailTextField.setWidth("100%");
		mailTextField.setId("createUserMailField");
		formLayout.addComponent(mailTextField);

		passwordField = new PasswordField(I18n.getMessage("admin.um.create.input.password"));
		passwordField.setWidth("100%");
		passwordField.setId("createUserPasswordField");
		formLayout.addComponent(passwordField);

		confirmPasswordField = new PasswordField(I18n.getMessage("admin.um.create.input.confirmpassword"));
		confirmPasswordField.setWidth("100%");
		confirmPasswordField.setId("createUserPasswordConfirmationField");
		formLayout.addComponent(confirmPasswordField);

		postalAddressField = new TextArea(I18n.getMessage("admin.um.create.input.address"));
		postalAddressField.setWidth("100%");
		postalAddressField.setId("createUserAddressField");
		formLayout.addComponent(postalAddressField);

		roleSelection = new ComboBox(I18n.getMessage("admin.um.create.input.role"));
		roleSelection.setNullSelectionAllowed(false);
		roleSelection.setInvalidAllowed(false);
		roleSelection.setWidth("100%");
		roleSelection.setId("createUserRoleSelect");
		formLayout.addComponent(roleSelection);
		// add selection items
		roleSelection.addItem(UserRoles.ADMINISTRATOR);
		roleSelection.setItemCaption(UserRoles.ADMINISTRATOR, I18n.getMessage("userrole." + UserRoles.ADMINISTRATOR));
		roleSelection.addItem(UserRoles.TRANSLATOR);
		roleSelection.setItemCaption(UserRoles.TRANSLATOR, I18n.getMessage("userrole." + UserRoles.TRANSLATOR));
		roleSelection.addItem(UserRoles.INTERNATIONAL_ORGANISATION);
		roleSelection.setItemCaption(UserRoles.INTERNATIONAL_ORGANISATION,
				I18n.getMessage("userrole." + UserRoles.INTERNATIONAL_ORGANISATION));
		roleSelection.addItem(UserRoles.REGIONAL_ORGANISATION);
		roleSelection.setItemCaption(UserRoles.REGIONAL_ORGANISATION,
				I18n.getMessage("userrole." + UserRoles.REGIONAL_ORGANISATION));
		// set default value
		roleSelection.setValue(UserRoles.REGIONAL_ORGANISATION);

		// layout to arrange the save/cancel button horizontal
		HorizontalLayout buttonLayout = new HorizontalLayout();
		buttonLayout.setSpacing(true);
		addComponent(buttonLayout);

		// save Button
		Button commitMessageButton = new Button(I18n.getMessage("admin.um.create.commit"));
		commitMessageButton.setId("createUserCommitButton");
		buttonLayout.addComponent(commitMessageButton);
		commitMessageButton.addClickListener(this); // TODO

		// cancel Button
		Button cancelButton = new Button(I18n.getMessage("admin.um.create.cancel"));
		cancelButton.setId("createUserCancelButton");
		buttonLayout.addComponent(cancelButton);
		cancelButton.addClickListener(new ClickListener() {

			@Override
			public void buttonClick(ClickEvent event) {
				// listener.cancel(); //TODO
			}
		});

	}

	private User getUser() {
		User user = new User();

		boolean valid = true;
		// name
		if (!Strings.isNullOrEmpty(nameTextField.getValue())) {
			nameTextField.setComponentError(null);
			user.setName(nameTextField.getValue());
		} else {
			nameTextField.setComponentError(new UserError(I18n.getMessage("admin.um.create.error.required")));
			valid = false;
		}

		// password
		if (!Strings.isNullOrEmpty(passwordField.getValue())) {
			passwordField.setComponentError(null);
		} else {
			passwordField.setComponentError(new UserError(I18n.getMessage("admin.um.create.error.required")));
			valid = false;
		}

		// password confirm
		if (!Strings.isNullOrEmpty(confirmPasswordField.getValue())) {
			confirmPasswordField.setComponentError(null);
		} else {
			confirmPasswordField.setComponentError(new UserError(I18n.getMessage("admin.um.create.error.required")));
			valid = false;
		}

		// mail
		if (!Strings.isNullOrEmpty(mailTextField.getValue())) {
			mailTextField.setComponentError(null);
			user.seteMail(mailTextField.getValue());
		} else {
			mailTextField.setComponentError(new UserError(I18n.getMessage("admin.um.create.error.required")));
			valid = false;
		}

		// mail
		if (!Strings.isNullOrEmpty(mailTextField.getValue())) {
			mailTextField.setComponentError(null);
			user.seteMail(mailTextField.getValue());
		} else {
			mailTextField.setComponentError(new UserError(I18n.getMessage("admin.um.create.error.required")));
			valid = false;
		}

		// address
		if (!Strings.isNullOrEmpty(postalAddressField.getValue())) {
			postalAddressField.setComponentError(null);
			user.setPostalAddress(postalAddressField.getValue());
		} else {
			postalAddressField.setComponentError(new UserError(I18n.getMessage("admin.um.create.error.required")));
			valid = false;
		}

		// role
		user.setRole((String) roleSelection.getValue());

		// password & password confirm
		if (!Strings.isNullOrEmpty(confirmPasswordField.getValue())
				&& !Strings.isNullOrEmpty(passwordField.getValue())) {
			// check if password inputs match
			if (passwordField.getValue().equals(confirmPasswordField.getValue())) {
				user.setPassword(passwordField.getValue());
			} else {
				passwordField.setComponentError(new UserError(I18n.getMessage("admin.um.create.error.nomatch")));
				confirmPasswordField.setComponentError(new UserError(I18n.getMessage("admin.um.create.error.nomatch")));
				valid = false;
			}
		}

		if (!valid) {
			user = null;
		}
		return user;
	}

	@Override
	public void buttonClick(ClickEvent event) {
		User user = getUser();
		if (user != null) {
			listener.saveUser(user);
		}
	}

	public interface CreateNewUserViewListener {
		void saveUser(User user);

		void cancel();
	}
}
