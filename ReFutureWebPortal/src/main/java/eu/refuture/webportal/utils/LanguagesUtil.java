package eu.refuture.webportal.utils;

import java.util.ArrayList;
import java.util.List;

/**
 * Util to get the codes of the supported localization languages
 * 
 * @author Europe Without Borders e.V.
 * @version 1.0
 */
public class LanguagesUtil {
	public static List<String> getLanguageCodes() {
		List<String> languageCodes = new ArrayList<String>();
		languageCodes.add("en");
		languageCodes.add("de");
		languageCodes.add("fr");
		languageCodes.add("ar");
		// Collections.sort(languageCodes);
		return languageCodes;
	}

}
