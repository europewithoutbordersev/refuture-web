package eu.refuture.webportal;

import javax.servlet.annotation.WebServlet;

import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.annotations.Widgetset;
import com.vaadin.server.Responsive;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.UI;

import eu.refuture.webportal.authentication.AccessControl;
import eu.refuture.webportal.authentication.BasicAccessControl;
import eu.refuture.webportal.authentication.CurrentUser;
import eu.refuture.webportal.authentication.LoginView;
import eu.refuture.webportal.authentication.LoginView.LoginViewListener;
import eu.refuture.webportal.data.DataUtils;
import eu.refuture.webportal.i18n.I18n;
import eu.refuture.webportal.menu.MainMenu;

/**
 * Central "Entry point" of the portal application.
 * 
 * @author Europe Without Borders e.V.
 * @version 1.0
 */
@SuppressWarnings("serial")
@Theme("refuturetheme")
@Widgetset("eu.refuture.ReFutureWebPortal.ReFutureWidgetset")
public class ReFutureUI extends UI {

	private AccessControl accessControl;

	@Override
	protected void init(VaadinRequest vaadinRequest) {
		// make the vaadin page responsive
		Responsive.makeResponsive(this);
		setLocale(vaadinRequest.getLocale());
		VaadinSession.getCurrent().setLocale(vaadinRequest.getLocale());
		accessControl = new BasicAccessControl();
		// set page title
		getPage().setTitle(I18n.getMessage("portal.title"));

		// check if user is logged in
		if (accessControl.isUserSignedIn()) {
			showMainView();
		} else {
			showLogin();
		}
	}

	private void showLogin() {
		// show the login view
		setContent(new LoginView(accessControl, new LoginViewListener() {

			@Override
			public void loginSuccessful() {
				showMainView();
			}
		}));
	}

	private void showMainView() {

		setContent(new MainMenu(DataUtils.getDatabaseUtility().getUserByMail(CurrentUser.get())));
	}

	@WebServlet(urlPatterns = "/*", name = "ReFutureUIServlet", asyncSupported = true)
	@VaadinServletConfiguration(ui = ReFutureUI.class, productionMode = false)
	public static class ReFutureUIServlet extends VaadinServlet {
	}
}
