package eu.refuture.webportal.data;

import java.util.Date;
import java.util.List;

import com.netflix.config.DynamicBooleanProperty;
import com.netflix.config.DynamicPropertyFactory;

import eu.refuture.dbaccess.DatabaseAccess;
import eu.refuture.dbaccess.IDatabaseAccess;
import eu.refuture.dbaccess.MockDatabaseAccess;
import eu.refuture.model.country.Country;
import eu.refuture.model.news.News;
import eu.refuture.model.poi.PointOfInterest;
import eu.refuture.model.poi.PointOfInterestType;
import eu.refuture.model.user.User;
import eu.refuture.webportal.authentication.CurrentUser;

/**
 * Util class for easy data-access
 * 
 * @author Europe Without Borders e.V.
 * @version 1.0
 */
public class DatabaseUtility {
	private IDatabaseAccess dbAccess;

	public DatabaseUtility() {
		DynamicBooleanProperty mockDbAccessProperty = DynamicPropertyFactory.getInstance()
				.getBooleanProperty("mock.dataaccess", false);
		if (mockDbAccessProperty.get()) {
			System.out.println(":) :) :) :) mock db access");
			// use a mocked db access
			dbAccess = new MockDatabaseAccess();
		} else {
			dbAccess = new DatabaseAccess();
		}
	}

	public User getUserByMail(String mail) {
		return dbAccess.getUserByMail(mail);
	}

	public void sendNews(News news) {
		news.setCreatedAt(new Date());
		news.setCreatedBy(getUserByMail(CurrentUser.get()));
		dbAccess.saveNews(news);
	}

	public void saveUser(User user) {
		dbAccess.saveUser(user);
	}

	public boolean authenticateUser(String mail, String password) {
		return dbAccess.authenticateUser(mail, password);
	}

	public List<Country> getCountries() {
		return dbAccess.getCountries();
	}

	public void savePoi(PointOfInterest poi) {
		poi.setCreatedAt(new Date());
		poi.setCreatedBy(getUserByMail(CurrentUser.get()));
		dbAccess.savePoi(poi);
	}

	public List<PointOfInterestType> getPoiTypes() {
		return dbAccess.getPoiTypes();
	}

	public List<News> getAllNews() {
		return dbAccess.getNews(new Date(0), "en", "de", "fr", "ar");
	}

	public PointOfInterest getPoiById(long id) {
		return dbAccess.getPoiById(id);
	}

	public List<PointOfInterest> getAllPois() {
		return dbAccess.getPois(new Date(0));
	}
}
