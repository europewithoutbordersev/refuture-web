package eu.refuture.webportal.data;

/**
 * Util class which provides an easy static access to the database util
 * 
 * @author Europe Without Borders e.V.
 * @version 1.0
 */
public class DataUtils {
	private static DatabaseUtility dbAccess = new DatabaseUtility();

	public static DatabaseUtility getDatabaseUtility() {
		return dbAccess;
	}
}
