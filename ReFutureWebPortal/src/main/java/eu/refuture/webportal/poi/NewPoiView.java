package eu.refuture.webportal.poi;

import java.util.List;
import java.util.Map;

import com.google.gwt.thirdparty.guava.common.collect.Maps;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.server.Page;
import com.vaadin.server.ThemeResource;
import com.vaadin.server.VaadinSession;
import com.vaadin.tapio.googlemaps.client.LatLon;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Panel;
import com.vaadin.ui.Slider;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

import eu.refuture.model.poi.PointOfInterest;
import eu.refuture.model.poi.PointOfInterestLocalization;
import eu.refuture.model.poi.PointOfInterestType;
import eu.refuture.webportal.data.DataUtils;
import eu.refuture.webportal.i18n.I18n;
import eu.refuture.webportal.news.regional.LatLonSelection;
import eu.refuture.webportal.utils.LanguagesUtil;

/**
 * A view to create an new Point of interest.
 * 
 * @author Europe Without Borders e.V.
 * @version 1.0
 */
@SuppressWarnings("serial")
public class NewPoiView extends VerticalLayout implements ClickListener {

	private NewPoiViewListener listener;
	private Map<String, PoiLocalizationComponent> localizations;
	private LatLonSelection latLonSelection;
	private Slider ratioField;
	private Panel localizationPanel = new Panel();
	private ComboBox languageSelect;
	private ComboBox poiTypeSelect;

	public NewPoiView(NewPoiViewListener componentListener) {
		super();
		this.listener = componentListener;
		// set layout spacing & margin
		setSpacing(true);
		setMargin(true);

		// create map for localization components
		localizations = Maps.newHashMap();

		// Title
		Label title = new Label(I18n.getMessage("poi.create.title"));
		title.addStyleName(ValoTheme.LABEL_H1);
		addComponent(title);

		// Lat lon selection
		latLonSelection = new LatLonSelection();
		latLonSelection.setCaption(I18n.getMessage("poi.create.latlon.title"));
		addComponent(latLonSelection);

		// poi-type selection
		poiTypeSelect = new ComboBox(I18n.getMessage("poi.create.type.title"));
		poiTypeSelect.setInvalidAllowed(false);
		poiTypeSelect.setNullSelectionAllowed(false);
		poiTypeSelect.setWidth("100%");
		addComponent(poiTypeSelect);
		List<PointOfInterestType> poiTypes = DataUtils.getDatabaseUtility().getPoiTypes();
		if (poiTypes != null) {
			for (PointOfInterestType type : poiTypes) {
				poiTypeSelect.addItem(type);
				poiTypeSelect.setItemCaption(type, I18n.getMessage("poi.type." + type.getId()));
			}
			if (!poiTypes.isEmpty()) {
				poiTypeSelect.setValue(poiTypes.get(0));
			}
		}

		// select input language
		languageSelect = new ComboBox(I18n.getMessage("poi.create.select.language"));
		languageSelect.addValueChangeListener(new ValueChangeListener() {

			@Override
			public void valueChange(ValueChangeEvent event) {
				Component c = localizations.get(languageSelect.getValue());
				if (c != null) {
					localizationPanel.setContent(c);
				}
			}
		});
		languageSelect.setImmediate(true);
		languageSelect.setInvalidAllowed(false);
		languageSelect.setNullSelectionAllowed(false);
		languageSelect.setWidth("100%");
		addComponent(languageSelect);
		for (String langCode : LanguagesUtil.getLanguageCodes()) {
			languageSelect.addItem(langCode);
			languageSelect.setItemCaption(langCode, I18n.getMessage("lang." + langCode));
			// no flag if lang is arabic :)
			if (!"ar".equals(langCode)) {
				languageSelect.setItemIcon(langCode, new ThemeResource("img/flagicons/" + langCode + ".png"));
			}

			localizations.put(langCode, new PoiLocalizationComponent(langCode));
		}
		if (languageSelect.containsId(VaadinSession.getCurrent().getLocale().getLanguage())) {
			languageSelect.setValue(VaadinSession.getCurrent().getLocale().getLanguage());
		} else {
			languageSelect.setValue("en");
		}

		// localization
		localizationPanel.addStyleName(ValoTheme.PANEL_BORDERLESS);
		localizationPanel.setWidth("100%");
		addComponent(localizationPanel);
		localizationPanel.setContent(localizations.get(languageSelect.getValue()));

		// ratio (-1 is "null")
		ratioField = new Slider(I18n.getMessage("poi.create.ratio"), -1, 100);
		ratioField.setValue(-1.0);
		ratioField.setWidth("100%");
		addComponent(ratioField);

		// layout to arrange the save/cancel button horizontal
		HorizontalLayout buttonLayout = new HorizontalLayout();
		buttonLayout.setSpacing(true);
		addComponent(buttonLayout);

		// save Button
		Button commitMessageButton = new Button(I18n.getMessage("poi.create.commit"));
		buttonLayout.addComponent(commitMessageButton);
		commitMessageButton.addClickListener(this);

		// cancel Button
		Button cancelButton = new Button(I18n.getMessage("poi.create.cancel"));
		buttonLayout.addComponent(cancelButton);
		cancelButton.addClickListener(new ClickListener() {

			@Override
			public void buttonClick(ClickEvent event) {
				listener.cancel();
			}
		});
	}

	@Override
	public void buttonClick(ClickEvent event) {
		PointOfInterest poi = new PointOfInterest();
		for (PoiLocalizationComponent localizationComponent : localizations.values()) {
			PointOfInterestLocalization poiLocalization = localizationComponent.getLocalization();
			if (poiLocalization != null) {
				poiLocalization.setPointOfInterest(poi);
				poi.addLocalization(poiLocalization);
			}
		}

		if (poi.getLocalizations() == null || poi.getLocalizations().isEmpty()) {
			// no localizations --> show error
			Notification notification = new Notification(I18n.getMessage("poi.create.error.missinglocalization.title"),
					I18n.getMessage("poi.create.error.missinglocalization.message"), Notification.Type.ERROR_MESSAGE);
			notification.setDelayMsec(5000);
			notification.show(Page.getCurrent());
		} else {
			// get poi type
			poi.setType((PointOfInterestType) poiTypeSelect.getValue());

			// Lat, lon, radius
			LatLon latLon = latLonSelection.getLatLon();
			poi.setLatitude(latLon.getLat());
			poi.setLongitude(latLon.getLon());
			poi.setRadius(latLonSelection.getRadius());

			// ratio
			poi.setRatio(ratioField.getValue().intValue());

			listener.savePoi(poi);
		}
	}

	/**
	 * Listener for the NewPoiView
	 */
	public interface NewPoiViewListener {

		void savePoi(PointOfInterest poi);

		void cancel();

	}
}
