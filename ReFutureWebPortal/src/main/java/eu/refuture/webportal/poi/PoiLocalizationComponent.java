package eu.refuture.webportal.poi;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;

import com.google.common.base.Strings;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

import eu.refuture.model.poi.PointOfInterestLocalization;
import eu.refuture.webportal.i18n.I18n;

/**
 * Component for input of localized poi description
 * 
 * @author Europe Without Borders e.V.
 * @version 1.0
 */
@SuppressWarnings("serial")
public class PoiLocalizationComponent extends VerticalLayout {

	private String languageCode;
	private TextField nameField;
	private TextArea descriptionField;
	private TextField capacityField;

	public PoiLocalizationComponent(String langCode) {
		super();
		this.languageCode = langCode;

		setSpacing(true);

		nameField = new TextField(I18n.getMessage("poi.create.name"));
		nameField.setWidth("100%");
		addComponent(nameField);

		descriptionField = new TextArea(I18n.getMessage("poi.create.description"));
		descriptionField.setWidth("100%");
		addComponent(descriptionField);

		capacityField = new TextField(I18n.getMessage("poi.create.capacity"));
		capacityField.setWidth("100%");
		addComponent(capacityField);
	}

	public PointOfInterestLocalization getLocalization() {
		PointOfInterestLocalization poiLocalization = null;
		if (!Strings.isNullOrEmpty(nameField.getValue()) && !Strings.isNullOrEmpty(descriptionField.getValue())) {
			poiLocalization = new PointOfInterestLocalization();
			poiLocalization.setLanguage(languageCode);

			try {
				poiLocalization.setName(URLEncoder.encode(nameField.getValue(), "UTF-8"));
			} catch (UnsupportedEncodingException e) {
				// do nothing?
				// TODO logging :)
			}

			try {
				poiLocalization.setDescription(URLEncoder.encode(descriptionField.getValue(), "UTF-8"));
			} catch (UnsupportedEncodingException e) {
				// do nothing?
				// TODO logging :)
			}

			if (Strings.isNullOrEmpty(capacityField.getValue())) {
				poiLocalization.setCapacity("");
			} else {
				try {
					poiLocalization.setCapacity(URLEncoder.encode(capacityField.getValue(), "UTF-8"));
				} catch (UnsupportedEncodingException e) {
					// do nothing?
					// TODO logging :)
				}
			}
		}
		return poiLocalization;
	}

	public void setTexts(String name, String description, String capacity) {
		try {
			nameField.setValue(URLDecoder.decode(name, "UTF-8"));
		} catch (UnsupportedEncodingException e) {
			// do nothing?
			// TODO logging :)
		}

		try {
			descriptionField.setValue(URLDecoder.decode(description, "UTF-8"));
		} catch (UnsupportedEncodingException e) {
			// do nothing?
			// TODO logging :)
		}

		if (capacity == null) {
			capacity = "";
		}
		try {
			capacityField.setValue(URLDecoder.decode(capacity, "UTF-8"));
		} catch (UnsupportedEncodingException e) {
			// do nothing?
			// TODO logging :)
		}
	}

	@Override
	public void setReadOnly(boolean readOnly) {
		super.setReadOnly(readOnly);
		nameField.setReadOnly(readOnly);
		descriptionField.setReadOnly(readOnly);
		capacityField.setReadOnly(true);
	}

}
