package eu.refuture.webportal.poi;

import java.util.Collections;
import java.util.List;

import com.vaadin.tapio.googlemaps.GoogleMap;
import com.vaadin.tapio.googlemaps.client.LatLon;
import com.vaadin.tapio.googlemaps.client.events.MarkerClickListener;
import com.vaadin.tapio.googlemaps.client.overlays.GoogleMapMarker;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

import eu.refuture.model.poi.PointOfInterest;
import eu.refuture.model.poi.PointOfInterestLocalization;
import eu.refuture.webportal.data.DataUtils;
import eu.refuture.webportal.i18n.I18n;

/**
 * View to display all availables points of interest on a map. If a point of
 * interest on the map is clicked, additional information about the poi is
 * displayed below the map.
 * 
 * @author Europe Without Borders e.V.
 * @version 1.0
 */
@SuppressWarnings("serial")
public class PoiOverview extends VerticalLayout {

	private GoogleMap googleMap = null;
	private Panel poiDetailsPanel = null;

	public PoiOverview() {
		super();
		setSpacing(true);
		setMargin(true);

		// Title
		Label title = new Label(I18n.getMessage("poi.overview.title"));
		title.addStyleName(ValoTheme.LABEL_H1);
		addComponent(title);

		googleMap = new GoogleMap("apiKey", null, "english");
		googleMap.setWidth("100%");
		googleMap.setHeight("400px");
		googleMap.setCenter(new LatLon(48.0501442, 8.2014192));
		googleMap.addMarkerClickListener(new MarkerClickListener() {

			@Override
			public void markerClicked(GoogleMapMarker clickedMarker) {
				showPoiDetails(clickedMarker.getId());

			}

		});
		addComponent(googleMap);

		poiDetailsPanel = new Panel();
		poiDetailsPanel.setVisible(false);
		poiDetailsPanel.setWidth("100%");
		addComponent(poiDetailsPanel);
	}

	public void setPois(List<PointOfInterest> pois) {
		googleMap.clearMarkers();
		for (PointOfInterest poi : pois) {
			GoogleMapMarker marker = new GoogleMapMarker(I18n.getMessage("general.poi") + " " + poi.getId(),
					new LatLon(poi.getLatitude(), poi.getLongitude()), false);
			marker.setId(poi.getId());
			googleMap.addMarker(marker);
		}
	}

	private void showPoiDetails(long id) {
		PointOfInterest poi = DataUtils.getDatabaseUtility().getPoiById(id);
		VerticalLayout detailsLayout = new VerticalLayout();
		detailsLayout.setSpacing(true);
		detailsLayout.setMargin(true);

		Label detailsCaptionLabel = new Label(I18n.getMessage("poi.overview.details.title"));
		detailsCaptionLabel.addStyleName(ValoTheme.LABEL_H2);
		detailsLayout.addComponent(detailsCaptionLabel);

		TextField idField = new TextField(I18n.getMessage("general.id"));
		idField.setWidth("100%");
		idField.setValue(String.valueOf(id));
		idField.setReadOnly(true);
		detailsLayout.addComponent(idField);

		TextField typeField = new TextField(I18n.getMessage("poi.create.type.title"));
		typeField.setWidth("100%");
		typeField.setValue(I18n.getMessage("poi.type." + poi.getType().getId()));
		typeField.setReadOnly(true);
		detailsLayout.addComponent(typeField);

		List<PointOfInterestLocalization> localizations = poi.getLocalizations();
		Collections.sort(localizations);
		for (PointOfInterestLocalization localization : localizations) {
			Panel localizationPanel = new Panel(localization.getLanguage());
			detailsLayout.addComponent(localizationPanel);

			PoiLocalizationComponent localizationComponent = new PoiLocalizationComponent(localization.getLanguage());
			localizationComponent.setMargin(true);
			localizationComponent.setTexts(localization.getName(), localization.getDescription(),
					localization.getCapacity());
			localizationComponent.setReadOnly(true);
			localizationPanel.setContent(localizationComponent);
		}

		poiDetailsPanel.setVisible(true);
		poiDetailsPanel.setContent(detailsLayout);
	}

}
