package eu.refuture.webportal.menu.commands;

import com.vaadin.server.Page;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.MenuBar.MenuItem;

/**
 * Contains the logic for the event, that a user wants to logout
 * @author Europe Without Borders e.V.
 * @version 1.0
 */
@SuppressWarnings("serial")
public class LogoutCommand implements MenuBar.Command{

	@Override
	public void menuSelected(MenuItem selectedItem) {
		VaadinSession.getCurrent().getSession().invalidate();
		Page.getCurrent().reload();
	}

}
