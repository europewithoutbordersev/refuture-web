package eu.refuture.webportal.menu.commands;

import com.vaadin.ui.MenuBar;
import com.vaadin.ui.MenuBar.MenuItem;

import eu.refuture.webportal.data.DataUtils;
import eu.refuture.webportal.menu.MainMenu;
import eu.refuture.webportal.news.NewsList;

/**
 * Contains the logic for the event that a user wants to get a list of all news
 * displayed
 * 
 * @author Europe Without Borders e.V.
 * @version 1.0
 */
@SuppressWarnings("serial")
public class NewsListCommand implements MenuBar.Command {

	private NewsList newsList = null;
	private MainMenu contentPanel;

	public NewsListCommand(MainMenu contentPanel) {
		super();
		this.contentPanel = contentPanel;
		this.newsList = new NewsList();
	}

	@Override
	public void menuSelected(MenuItem selectedItem) {
		newsList.setData(DataUtils.getDatabaseUtility().getAllNews());
		contentPanel.setContent(newsList);
	}

}
