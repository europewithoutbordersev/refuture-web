package eu.refuture.webportal.menu.commands;

import com.vaadin.ui.MenuBar;
import com.vaadin.ui.MenuBar.MenuItem;

import eu.refuture.webportal.data.DataUtils;
import eu.refuture.webportal.menu.MainMenu;
import eu.refuture.webportal.poi.PoiOverview;

/**
 * Contains logic for the event, that a user wants to get an overview over all
 * Points of interest
 * 
 * @author Europe Without Borders e.V.
 * @version 1.0
 */
@SuppressWarnings("serial")
public class PoiOverviewCommand implements MenuBar.Command {

	private PoiOverview poiOverview = null;
	private MainMenu contentPanel;

	public PoiOverviewCommand(MainMenu contentPanel) {
		super();
		this.contentPanel = contentPanel;
		poiOverview = new PoiOverview();
	}

	@Override
	public void menuSelected(MenuItem selectedItem) {
		// TODO Auto-generated method stub
		poiOverview.setPois(DataUtils.getDatabaseUtility().getAllPois());
		contentPanel.setContent(poiOverview);
	}

}
