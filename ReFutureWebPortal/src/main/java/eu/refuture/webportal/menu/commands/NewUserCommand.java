package eu.refuture.webportal.menu.commands;

import com.vaadin.server.Page;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.Notification;
import com.vaadin.ui.MenuBar.MenuItem;

import eu.refuture.model.user.User;
import eu.refuture.webportal.administration.usermanagement.CreateNewUserView;
import eu.refuture.webportal.administration.usermanagement.CreateNewUserView.CreateNewUserViewListener;
import eu.refuture.webportal.data.DataUtils;
import eu.refuture.webportal.i18n.I18n;
import eu.refuture.webportal.menu.MainMenu;

/**
 * Contains the logic for the event that a user wants to create a new user
 * account for the webportal
 * 
 * @author Europe Without Borders e.V.
 * @version 1.0
 */
@SuppressWarnings("serial")
public class NewUserCommand implements MenuBar.Command {
	private MainMenu contentPanel;

	public NewUserCommand(MainMenu contentPanel) {
		this.contentPanel = contentPanel;
	}

	@Override
	public void menuSelected(MenuItem selectedItem) {
		contentPanel.setContent(new CreateNewUserView(new CreateNewUserViewListener() {

			@Override
			public void saveUser(User user) {
				try {
					DataUtils.getDatabaseUtility().saveUser(user);
					contentPanel.setContent(null);
					Notification notification = new Notification(I18n.getMessage("admin.um.create.success.title"),
							I18n.getMessage("admin.um.create.success.message"), Notification.Type.HUMANIZED_MESSAGE);
					notification.setDelayMsec(5000);
					notification.show(Page.getCurrent());
				} catch (IllegalArgumentException e) {
					Notification notification = new Notification(
							I18n.getMessage("admin.um.create.error.mailtaken.title"),
							I18n.getMessage("admin.um.create.error.mailtaken.message"),
							Notification.Type.HUMANIZED_MESSAGE);
					notification.setDelayMsec(5000);
					notification.show(Page.getCurrent());
				} catch (Exception e) {
					Notification notification = new Notification(I18n.getMessage("portal.error.unknown.title"),
							I18n.getMessage("portal.error.unknown.message"), Notification.Type.HUMANIZED_MESSAGE);
					notification.setDelayMsec(5000);
					notification.show(Page.getCurrent());
				}
			}

			@Override
			public void cancel() {
				// TODO Auto-generated method stub
				contentPanel.setContent(null);
			}
		}));
	}
}
