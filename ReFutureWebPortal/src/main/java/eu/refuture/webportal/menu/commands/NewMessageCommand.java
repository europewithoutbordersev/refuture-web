package eu.refuture.webportal.menu.commands;

import com.vaadin.server.Page;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.Notification;
import com.vaadin.ui.MenuBar.MenuItem;

import eu.refuture.model.news.News;
import eu.refuture.model.user.User;
import eu.refuture.webportal.data.DataUtils;
import eu.refuture.webportal.i18n.I18n;
import eu.refuture.webportal.menu.MainMenu;
import eu.refuture.webportal.news.NewNewsView;
import eu.refuture.webportal.news.NewNewsView.NewNewsViewListener;

/**
 * COntaints the logic for the event that a user wants to create an new news
 * 
 * @author Europe Without Borders e.V.
 * @version 1.0
 */
@SuppressWarnings("serial")
public class NewMessageCommand implements MenuBar.Command {

	private User user;
	private MainMenu contentPanel;

	public NewMessageCommand(User user, MainMenu contentPanel) {
		this.user = user;
		this.contentPanel = contentPanel;
	}

	@Override
	public void menuSelected(MenuItem selectedItem) {
		contentPanel.setContent(new NewNewsView(user, new NewNewsViewListener() {

			@Override
			public void sendNews(News news) {
				DataUtils.getDatabaseUtility().sendNews(news);
				contentPanel.setContent(null);
				Notification notification = new Notification(I18n.getMessage("news.create.success.title"),
						I18n.getMessage("news.create.success.message"), Notification.Type.HUMANIZED_MESSAGE);
				notification.setDelayMsec(5000);
				notification.show(Page.getCurrent());
			}

			@Override
			public void cancel() {
				contentPanel.setContent(null);
			}
		}));
	}

}
