package eu.refuture.webportal.menu.commands;

import com.vaadin.server.Page;
import com.vaadin.ui.Component;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.MenuBar.MenuItem;
import com.vaadin.ui.Notification;

import eu.refuture.model.poi.PointOfInterest;
import eu.refuture.webportal.data.DataUtils;
import eu.refuture.webportal.i18n.I18n;
import eu.refuture.webportal.menu.MainMenu;
import eu.refuture.webportal.poi.NewPoiView;
import eu.refuture.webportal.poi.NewPoiView.NewPoiViewListener;

/**
 * Contains the logic for the event that a user wants to create a new point of
 * interest
 * 
 * @author Europe Without Borders e.V.
 * @version 1.0
 */
@SuppressWarnings("serial")
public class NewPoiCommand implements MenuBar.Command {
	private MainMenu contentPanel;

	public NewPoiCommand(MainMenu contentPanel) {
		super();
		this.contentPanel = contentPanel;
	}

	@Override
	public void menuSelected(MenuItem selectedItem) {
		NewPoiViewListener componentListener = new NewPoiViewListener() {

			@Override
			public void savePoi(PointOfInterest poi) {
				DataUtils.getDatabaseUtility().savePoi(poi);
				contentPanel.setContent(null);
				Notification notification = new Notification(I18n.getMessage("poi.create.success.title"),
						I18n.getMessage("poi.create.success.message"), Notification.Type.HUMANIZED_MESSAGE);
				notification.setDelayMsec(5000);
				notification.show(Page.getCurrent());
			}

			@Override
			public void cancel() {
				contentPanel.setContent(null);
			}
		};
		Component c = new NewPoiView(componentListener);
		contentPanel.setContent(c);

	}

}
