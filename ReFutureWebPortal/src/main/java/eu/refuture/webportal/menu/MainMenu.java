package eu.refuture.webportal.menu;

import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Component;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.MenuBar.MenuItem;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;

import eu.refuture.model.user.User;
import eu.refuture.model.user.UserRoles;
import eu.refuture.webportal.i18n.I18n;
import eu.refuture.webportal.menu.commands.LogoutCommand;
import eu.refuture.webportal.menu.commands.NewMessageCommand;
import eu.refuture.webportal.menu.commands.NewPoiCommand;
import eu.refuture.webportal.menu.commands.NewUserCommand;
import eu.refuture.webportal.menu.commands.NewsListCommand;
import eu.refuture.webportal.menu.commands.PoiOverviewCommand;

/**
 * Main view of the application. Contains the main menu bar on top and a panel
 * for the content to display.
 * 
 * @author Europe Without Borders e.V.
 * @version 1.0
 */
@SuppressWarnings("serial")
public class MainMenu extends VerticalLayout {

	private Panel contentPanel = new Panel();

	public MainMenu(User user) {
		super();
		setWidth("100%");
		setMargin(true);
		setSpacing(true);

		// Menu bar on the top
		MenuBar menuBar = new MenuBar();
		menuBar.setHeightUndefined();
		menuBar.setId("mainMenuBar");
		addComponent(menuBar);
		setComponentAlignment(menuBar, Alignment.TOP_CENTER);

		// User menu
		MenuItem userMenu = menuBar.addItem(user.getName(), FontAwesome.USER, null);
		userMenu.addItem(I18n.getMessage("menu.user.logout"), FontAwesome.SIGN_OUT, new LogoutCommand()); // TODO

		// News
		MenuItem newsMenu = menuBar.addItem(I18n.getMessage("menu.news"), FontAwesome.ENVELOPE, null);
		newsMenu.addItem(I18n.getMessage("menu.news.new"), FontAwesome.PLUS_SQUARE_O,
				new NewMessageCommand(user, this));
		newsMenu.addItem(I18n.getMessage("menu.news.list"), FontAwesome.LIST, new NewsListCommand(this));

		// POIs
		MenuItem poiMenu = menuBar.addItem(I18n.getMessage("menu.poi"), FontAwesome.MAP_MARKER, null);
		poiMenu.addItem(I18n.getMessage("menu.poi.new"), FontAwesome.PLUS_SQUARE_O, new NewPoiCommand(this));
		poiMenu.addItem(I18n.getMessage("menu.poi.overview"), FontAwesome.MAP_MARKER, new PoiOverviewCommand(this));

		if (UserRoles.ADMINISTRATOR.equals(user.getRole())) {

			// Administration
			MenuItem administrationMenu = menuBar.addItem(I18n.getMessage("menu.admin"), FontAwesome.COG, null);

			MenuItem userManagementMenu = administrationMenu.addItem(I18n.getMessage("menu.admin.um"),
					FontAwesome.USERS, null);
			userManagementMenu.addItem(I18n.getMessage("menu.admin.um.new"), new NewUserCommand(this));
		}

		contentPanel.setWidth("100%");
		addComponent(contentPanel);
		setComponentAlignment(contentPanel, Alignment.TOP_CENTER);

	}

	public void setContent(Component component) {
		contentPanel.setContent(component);
	}
}
