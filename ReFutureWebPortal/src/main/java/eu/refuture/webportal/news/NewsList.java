package eu.refuture.webportal.news;

import java.util.Collections;
import java.util.Date;
import java.util.List;

import com.google.common.base.Function;
import com.google.common.base.Joiner;
import com.google.common.collect.Lists;
import com.google.gwt.thirdparty.guava.common.base.Strings;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.event.ItemClickEvent.ItemClickListener;
import com.vaadin.shared.data.sort.SortDirection;
import com.vaadin.shared.ui.grid.HeightMode;
import com.vaadin.ui.Component;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Grid.DetailsGenerator;
import com.vaadin.ui.Grid.RowReference;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

import eu.refuture.model.news.News;
import eu.refuture.model.news.NewsLocalization;
import eu.refuture.webportal.i18n.I18n;

/**
 * View to display all available news.
 * 
 * @author Europe Without Borders e.V.
 * @version 1.0
 */
@SuppressWarnings("serial")
public class NewsList extends VerticalLayout {

	private Grid newsGrid = null;

	public NewsList() {
		super();
		setSpacing(true);
		setMargin(true);

		buildUi();
	}

	private void buildUi() {
		// Title
		Label title = new Label(I18n.getMessage("news.list.title"));
		title.addStyleName(ValoTheme.LABEL_H1);
		addComponent(title);

		newsGrid = new Grid();
		newsGrid.setSizeFull();
		newsGrid.setHeightMode(HeightMode.ROW);
		// newsGrid.setColumns("id", "localizations", "latitudeLongitude",
		// "country", "createdAt");
		newsGrid.setColumnReorderingAllowed(true);
		newsGrid.addItemClickListener(getItemDetailsClickListener());
		addComponent(newsGrid);

	}

	private ItemClickListener getItemDetailsClickListener() {
		return new ItemClickListener() {

			@Override
			public void itemClick(ItemClickEvent event) {
				Object itemId = event.getItemId();
				newsGrid.setDetailsVisible(itemId, !newsGrid.isDetailsVisible(itemId));
			}
		};
	}

	public void setData(List<News> news) {
		Function<News, NewsListRow> rowTransforFunction = new Function<News, NewsListRow>() {
			public NewsListRow apply(News news) {
				return new NewsListRow(news);
			}
		};

		List<NewsListRow> newsRows = Lists.transform(news, rowTransforFunction);

		BeanItemContainer<NewsListRow> container = new BeanItemContainer<NewsListRow>(NewsListRow.class, newsRows);
		newsGrid.setContainerDataSource(container);
		newsGrid.setColumns("createdAt", "localizations", "latitudeLongitude", "country", "createdBy");
		newsGrid.setFrozenColumnCount(1);
		newsGrid.setDetailsGenerator(createDetailsGenerator());
		newsGrid.sort("createdAt", SortDirection.DESCENDING);
		if (news.size() < 20) {
			newsGrid.setHeightByRows(news.size());
		} else {
			newsGrid.setHeightByRows(20);
		}
	}

	private DetailsGenerator createDetailsGenerator() {
		return new DetailsGenerator() {

			@Override
			public Component getDetails(RowReference rowReference) {
				final NewsListRow row = (NewsListRow) rowReference.getItemId();

				Panel detailsPanel = new Panel();
				detailsPanel.setStyleName(ValoTheme.PANEL_BORDERLESS);

				VerticalLayout detailsLayout = new VerticalLayout();
				detailsLayout.setSpacing(true);
				detailsLayout.setMargin(true);
				detailsPanel.setContent(detailsLayout);

				List<NewsLocalization> localizations = row.getNews().getLocalizations();
				Collections.sort(localizations);
				for (NewsLocalization localization : localizations) {
					Panel localizationPanel = new Panel(localization.getLanguage());
					// localizationPanel.setSizeFull();
					localizationPanel.setWidth("100%");

					NewsLocalizationComponent localComponent = new NewsLocalizationComponent(
							localization.getLanguage());
					localComponent.setData(localization.getHeading(), localization.getBody());
					localComponent.setReadOnly(true);
					localComponent.setMargin(true);
					localizationPanel.setContent(localComponent);

					detailsLayout.addComponent(localizationPanel);
				}

				return detailsPanel;
			}
		};
	}

	public class NewsListRow {
		private News news;

		public NewsListRow(News news) {
			super();
			this.news = news;
		}

		public Long getId() {
			return news.getId();
		}

		public String getLocalizations() {
			List<String> localizationLangs = Lists.newArrayList();
			for (NewsLocalization localization : news.getLocalizations()) {
				localizationLangs.add(localization.getLanguage());
			}
			Collections.sort(localizationLangs);
			return Joiner.on(", ").join(localizationLangs);
		}

		public String getLatitudeLongitude() {
			double lat = news.getLatitude();
			double lon = news.getLongitude();

			String latLon = "";
			if (lat == 0 && lon == 0) {
				latLon = "-";
			} else {
				latLon = lat + " / " + lon;
			}
			return latLon;
		}

		public String getCountry() {
			String country = "";
			if (news.getCountry() == null) {
				country = "-";
			} else {
				country = news.getCountry().getId();
			}
			return country;
		}

		public Date getCreatedAt() {
			return news.getCreatedAt();
		}

		public String getCreatedBy() {
			String createdBy = "";
			if (news.getCreatedBy() != null && !Strings.isNullOrEmpty(news.getCreatedBy().getName())) {
				createdBy = news.getCreatedBy().getName();
			}
			return createdBy;
		}

		public News getNews() {
			return news;
		}
	}
}
