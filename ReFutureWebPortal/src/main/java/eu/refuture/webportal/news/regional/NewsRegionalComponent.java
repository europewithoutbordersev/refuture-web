package eu.refuture.webportal.news.regional;

import com.vaadin.ui.TabSheet;
import com.vaadin.ui.VerticalLayout;

import eu.refuture.model.country.Country;
import eu.refuture.webportal.i18n.I18n;

/**
 * Component for selecting the "region" in which a news is relevant (country OR
 * lat/lon).
 * 
 * @author Europe Without Borders e.V.
 * @version 1.0
 */
@SuppressWarnings("serial")
public class NewsRegionalComponent extends VerticalLayout {

	private TabSheet tabsheet;
	private LatLonSelection latLonSelection;
	private CountrySelection countrySelection;

	public NewsRegionalComponent() {
		super();
		setSpacing(true);
		setMargin(true);

		tabsheet = new TabSheet();
		tabsheet.setWidth("100%");
		addComponent(tabsheet);

		latLonSelection = new LatLonSelection();
		countrySelection = new CountrySelection();

		tabsheet.addTab(latLonSelection, I18n.getMessage("news.regional.latlong.title"));
		tabsheet.addTab(countrySelection, I18n.getMessage("news.regional.country.title"));

	}

	public boolean isLatLongSelected() {
		return tabsheet.getSelectedTab() instanceof LatLonSelection;
	}

	public double getLat() {
		return latLonSelection.getLatLon().getLat();
	}

	public double getLon() {
		return latLonSelection.getLatLon().getLon();
	}

	public int getRadius() {
		return latLonSelection.getRadius();
	}

	public Country getCountry() {
		return countrySelection.getSelectedCountry();
	}
}
