package eu.refuture.webportal.news.regional;

import com.vaadin.tapio.googlemaps.GoogleMap;
import com.vaadin.tapio.googlemaps.client.LatLon;
import com.vaadin.tapio.googlemaps.client.overlays.GoogleMapMarker;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

import eu.refuture.webportal.i18n.I18n;

/**
 * Component to select a location on a google-maps-map
 * 
 * @author Europe Without Borders e.V.
 * @version 1.0
 */
@SuppressWarnings("serial")
public class LatLonSelection extends VerticalLayout {

	private GoogleMapMarker marker;
	private TextField radius;

	public LatLonSelection() {
		super();
		setSpacing(true);

		GoogleMap googleMap = new GoogleMap("apiKey", null, "english");
		googleMap.setWidth("100%");
		googleMap.setHeight("400px");
		googleMap.setCenter(new LatLon(48.0501442, 8.2014192));
		addComponent(googleMap);

		marker = new GoogleMapMarker(I18n.getMessage("news.regional.latlong.marker"), new LatLon(48.0501442, 8.2014192),
				true);
		googleMap.addMarker(marker);

		radius = new TextField(I18n.getMessage("news.regional.latlong.radius"));
		radius.setId("newsRegionalLatLonRadiusField");
		radius.setWidth("100%");
		radius.setValue("1");
		// radius.addValidator(new
		// IntegerRangeValidator(I18n.getMessage("news.regional.latlong.radius.error"),
		// 1, 200)); // min 1km , max 200km
		// radius.setImmediate(true);
		addComponent(radius);
	}

	public LatLon getLatLon() {
		return marker.getPosition();
	}

	public int getRadius() {
		int radiusValue = 1;
		try {
			radiusValue = Integer.valueOf(radius.getValue());
		} catch (Throwable t) {
			// Do nothing
		}
		return radiusValue;
	}
}
