package eu.refuture.webportal.news;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;

import com.google.common.base.Strings;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

import eu.refuture.model.news.NewsLocalization;
import eu.refuture.webportal.i18n.I18n;

/**
 * View for input of a localized news title + message
 * 
 * @author Europe Without Borders e.V.
 * @version 1.0
 */
@SuppressWarnings("serial")
public class NewsLocalizationComponent extends VerticalLayout {

	private String languageCode;
	private TextField titleTextField;
	private TextArea messageTextArea;

	public NewsLocalizationComponent(String langCode) {
		super();
		this.languageCode = langCode;

		setSpacing(true);

		titleTextField = new TextField(
				I18n.getMessage("news.create.input.title") + " - " + I18n.getMessage("lang." + languageCode));
		titleTextField.setWidth("100%");
		addComponent(titleTextField);

		messageTextArea = new TextArea(
				I18n.getMessage("news.create.input.message") + " - " + I18n.getMessage("lang." + languageCode));
		messageTextArea.setWidth("100%");
		messageTextArea.setRows(10);
		addComponent(messageTextArea);
	}

	public NewsLocalization getLocalization() {
		NewsLocalization localization = null;

		// check if both input fields are filled
		if (!(Strings.isNullOrEmpty(messageTextArea.getValue()) && Strings.isNullOrEmpty(messageTextArea.getValue()))) {
			localization = new NewsLocalization();

			// set language code
			localization.setLanguage(languageCode);

			// set message title
			if (!Strings.isNullOrEmpty(titleTextField.getValue())) {
				try {
					localization.setHeading(URLEncoder.encode(titleTextField.getValue(), "UTF-8"));
				} catch (UnsupportedEncodingException e) {
					// do nothing?
				}
			}

			// set message
			if (!Strings.isNullOrEmpty(messageTextArea.getValue())) {
				try {
					localization.setBody(URLEncoder.encode(messageTextArea.getValue(), "UTF-8"));
				} catch (UnsupportedEncodingException e) {
					// do nothing?
				}
			}
		}

		return localization;
	}

	public void setData(String heading, String body) {
		try {
			titleTextField.setValue(URLDecoder.decode(heading, "UTF-8"));
		} catch (UnsupportedEncodingException e) {
			// do nothing?
			// TODO logging :)
		}

		try {
			messageTextArea.setValue(URLDecoder.decode(body, "UTF-8"));
		} catch (UnsupportedEncodingException e) {
			// do nothing?
			// TODO logging :)
		}
	}

	public void setReadOnly(boolean isReadOnly) {
		super.setReadOnly(true);
		titleTextField.setReadOnly(isReadOnly);
		messageTextArea.setReadOnly(isReadOnly);
	}
}
