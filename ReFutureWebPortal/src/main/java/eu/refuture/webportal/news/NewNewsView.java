package eu.refuture.webportal.news;

import java.util.HashMap;
import java.util.Map;

import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.server.Page;
import com.vaadin.server.ThemeResource;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

import eu.refuture.model.country.Country;
import eu.refuture.model.news.News;
import eu.refuture.model.news.NewsLocalization;
import eu.refuture.model.user.User;
import eu.refuture.model.user.UserRoles;
import eu.refuture.webportal.i18n.I18n;
import eu.refuture.webportal.news.regional.NewsRegionalComponent;
import eu.refuture.webportal.utils.LanguagesUtil;

/**
 * View to create a new news. Contains various inputs and selections. :)
 * 
 * @author Europe Without Borders e.V.
 * @version 1.0
 */
@SuppressWarnings("serial")
public class NewNewsView extends VerticalLayout implements Button.ClickListener {

	private CheckBox globalMessageCheckBox;
	private ComboBox languageSelect;
	private Panel localizationPanel = new Panel();
	private NewsRegionalComponent regionalComponent;

	private NewNewsViewListener listener;

	private Map<String, NewsLocalizationComponent> localizations;

	public NewNewsView(User user, NewNewsViewListener componentListener) {
		super();
		this.listener = componentListener;
		setMargin(true);
		setSpacing(true);

		localizations = new HashMap<String, NewsLocalizationComponent>();

		// Title
		Label title = new Label(I18n.getMessage("news.create.title"));
		title.addStyleName(ValoTheme.LABEL_H1);
		addComponent(title);

		// Send to all users or is the message regional?
		globalMessageCheckBox = new CheckBox(I18n.getMessage("news.create.global"));
		addComponent(globalMessageCheckBox);
		globalMessageCheckBox.setValue(false);
		globalMessageCheckBox.setImmediate(true);
		globalMessageCheckBox.addValueChangeListener(new ValueChangeListener() {

			@Override
			public void valueChange(ValueChangeEvent event) {
				regionalComponent.setVisible(!globalMessageCheckBox.getValue());
			}
		});

		if (!(UserRoles.ADMINISTRATOR.equals(user.getRole())
				|| UserRoles.INTERNATIONAL_ORGANISATION.equals(user.getRole()))) {
			globalMessageCheckBox.setEnabled(false);
		}

		Panel regionalPanel = new Panel();
		regionalComponent = new NewsRegionalComponent();
		regionalPanel.setContent(regionalComponent);
		addComponent(regionalPanel);

		// select input language
		languageSelect = new ComboBox(I18n.getMessage("news.create.select.language"));
		languageSelect.addValueChangeListener(new ValueChangeListener() {

			@Override
			public void valueChange(ValueChangeEvent event) {
				Component c = localizations.get(languageSelect.getValue());
				if (c != null) {
					localizationPanel.setContent(c);
				}
			}
		});
		languageSelect.setImmediate(true);
		languageSelect.setInvalidAllowed(false);
		languageSelect.setNullSelectionAllowed(false);
		languageSelect.setWidth("100%");
		addComponent(languageSelect);
		for (String langCode : LanguagesUtil.getLanguageCodes()) {
			languageSelect.addItem(langCode);
			languageSelect.setItemCaption(langCode, I18n.getMessage("lang." + langCode));
			if (!"ar".equals(langCode)) {
				languageSelect.setItemIcon(langCode, new ThemeResource("img/flagicons/" + langCode + ".png"));
			}

			localizations.put(langCode, new NewsLocalizationComponent(langCode));
		}
		if (languageSelect.containsId(VaadinSession.getCurrent().getLocale().getLanguage())) {
			languageSelect.setValue(VaadinSession.getCurrent().getLocale().getLanguage());
		} else {
			languageSelect.setValue("en");
		}

		// panel for textinput
		localizationPanel.addStyleName(ValoTheme.PANEL_BORDERLESS);
		addComponent(localizationPanel);
		System.out.println(languageSelect.getValue());
		localizationPanel.setContent(localizations.get(languageSelect.getValue()));

		// layout to arrange the save/cancel button horizontal
		HorizontalLayout buttonLayout = new HorizontalLayout();
		buttonLayout.setSpacing(true);
		addComponent(buttonLayout);

		// save Button
		Button commitMessageButton = new Button(I18n.getMessage("news.create.commit"));
		buttonLayout.addComponent(commitMessageButton);
		commitMessageButton.addClickListener(this);

		// cancel Button
		Button cancelButton = new Button(I18n.getMessage("news.create.cancel"));
		buttonLayout.addComponent(cancelButton);
		cancelButton.addClickListener(new ClickListener() {

			@Override
			public void buttonClick(ClickEvent event) {
				listener.cancel();
			}
		});
	}

	@Override
	public void buttonClick(ClickEvent event) {
		News news = new News();
		for (NewsLocalizationComponent localizationComponent : localizations.values()) {
			NewsLocalization localization = localizationComponent.getLocalization();
			if (localization != null) {
				news.addLocalization(localization);
			}
		}

		if (news.getLocalizations() == null || news.getLocalizations().isEmpty()) {
			// show error
			Notification notification = new Notification(I18n.getMessage("news.create.error.missinglocalization.title"),
					I18n.getMessage("news.create.error.missinglocalization.message"), Notification.Type.ERROR_MESSAGE);
			notification.setDelayMsec(5000);
			notification.show(Page.getCurrent());
		} else {
			if (globalMessageCheckBox.getValue()) {
				listener.sendNews(news);
			} else {
				if (regionalComponent.isLatLongSelected()) {
					// lat lon selection
					news.setLatitude(regionalComponent.getLat());
					news.setLongitude(regionalComponent.getLon());
					news.setRadius(regionalComponent.getRadius());
					listener.sendNews(news);
				} else {
					// country selection
					Country selectedCountry = regionalComponent.getCountry();
					if (selectedCountry == null) {

						Notification notification = new Notification("Invalid", "No valid country was selected.",
								Notification.Type.ERROR_MESSAGE);
						notification.setDelayMsec(5000);
						notification.show(Page.getCurrent());
					} else {
						news.setCountry(selectedCountry);
						listener.sendNews(news);
					}
				}
			}
		}
	}

	public interface NewNewsViewListener {
		public void cancel();

		public void sendNews(News news);
	}
}
