package eu.refuture.webportal.news.regional;

import java.util.List;

import com.vaadin.server.ThemeResource;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.VerticalLayout;

import eu.refuture.model.country.Country;
import eu.refuture.webportal.data.DataUtils;
import eu.refuture.webportal.i18n.I18n;

/**
 * Component to select a country from a list of countries
 * 
 * @author Europe Without Borders e.V.
 * @version 1.0
 */
@SuppressWarnings("serial")
public class CountrySelection extends VerticalLayout {

	private ComboBox countrySelection;

	public CountrySelection() {
		super();
		setSpacing(true);

		countrySelection = new ComboBox(I18n.getMessage("news.regional.country.selection"));
		countrySelection.setWidth("100%");
		countrySelection.setInvalidAllowed(false);
		countrySelection.setNullSelectionAllowed(false);
		addComponent(countrySelection);

		List<Country> countries = DataUtils.getDatabaseUtility().getCountries();
		for (Country country : countries) {
			countrySelection.addItem(country);
			countrySelection.setItemCaption(country, I18n.getMessage("country." + country.getId().toLowerCase()));
			countrySelection.setItemIcon(country,
					new ThemeResource("img/flagicons/" + country.getId().toLowerCase() + ".png"));
			if ("de".equals(country.getId().toLowerCase())) {
				countrySelection.setValue(country);
			}
		}
	}

	public Country getSelectedCountry() {
		return (Country) countrySelection.getValue();
	}

}
