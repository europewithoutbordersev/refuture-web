package eu.refuture.webportal.i18n;

import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Strings;
import com.vaadin.server.VaadinSession;

/**
 * Simple utility for Internationalization of the webportal. Loads texts from
 * .properties-files depending on the browsers locale
 * 
 * @author Europe Without Borders e.V.
 * @version 1.0
 */
public class I18n {
	private final static Logger LOGGER = LoggerFactory.getLogger(I18n.class);

	public static String getMessage(String key) {
		Locale locale = VaadinSession.getCurrent().getLocale();
		return getMessage(key, locale);
	}

	public static String getMessage(String key, Locale locale) {
		ResourceBundle textResources = ResourceBundle.getBundle("textResources", locale);

		String text;
		try {
			text = textResources.getString(key);
			if (Strings.isNullOrEmpty(text)) {
				text = key;
			}
		} catch (MissingResourceException e) {
			LOGGER.warn("No String found in resources for key: " + key);
			text = key;
		}
		return text;
	}

}
