package eu.refuture.model.country;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * @author Europe Without Borders e.V.
 * @version 1.0
 */
public class CountryTest {

	@Test
	public void testAddLocalization() {
		Country country = new Country();
		assertNull(country.getLocalizations());

		CountryLocalization localization = new CountryLocalization();
		localization.setLang("en");
		country.addLocalization(localization);
		assertNotNull(country.getLocalizations());
		assertTrue(country.getLocalizations().size() == 1);
	}

}
