package eu.refuture.model.news;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * @author Europe Without Borders e.V.
 * @version 1.0
 */
public class NewsTest {

	@Test
	public void testAddLocalization() {
		News news = new News();
		assertNull(news.getLocalizations());
		
		NewsLocalization localization = new NewsLocalization();
		localization.setLanguage("en");
		localization.setNews(news);
		news.addLocalization(localization);
		assertNotNull(news.getLocalizations());
		assertTrue(news.getLocalizations().size() == 1);
	}

}
