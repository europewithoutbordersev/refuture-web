package eu.refuture.model.generators;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

import eu.refuture.model.poi.PointOfInterestType;
import eu.refuture.model.poi.PointOfInterestTypeLocalization;

/**
 * @author Europe Without Borders e.V.
 * @version 1.0
 */
public class PoiTypeGeneratorTest {

	@Test
	public void testGetDefaultPoiTypes() {
		List<PointOfInterestType> types = PoiTypeGenerator.getDefaultPoiTypes();

		assertTrue(types.size() == 3);
		for (PointOfInterestType type : types) {
			assertTrue(type.getLocalizations().size() == 4);

			boolean containsDeLocalization = false;
			boolean containsEnLocalization = false;
			boolean containsFrLocalization = false;
			boolean containsArLocalization = false;
			for (PointOfInterestTypeLocalization localization : type.getLocalizations()) {
				assertNotNull(localization.getName());
				assertTrue(localization.getName().length() > 0);
				switch (localization.getLang()) {
				case "de":
					containsDeLocalization = true;
					break;
				case "en":
					containsEnLocalization = true;
					break;
				case "fr":
					containsFrLocalization = true;
					break;
				case "ar":
					containsArLocalization = true;
					break;

				}
			}

			assertTrue(containsDeLocalization);
			assertTrue(containsEnLocalization);
			assertTrue(containsFrLocalization);
			assertTrue(containsArLocalization);
		}
	}

}
