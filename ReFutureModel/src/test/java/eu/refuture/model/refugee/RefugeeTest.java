package eu.refuture.model.refugee;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 * @author Europe Without Borders e.V.
 * @version 1.0
 */
public class RefugeeTest {

	@Test
	public void testAddPosition() {
		Refugee refugee = new Refugee();
		assertNull(refugee.getPositions());
		
		RefugeePosition position = new RefugeePosition();
		refugee.addPosition(position);
		assertNotNull(refugee.getPositions());
		assertTrue(refugee.getPositions().size() == 1);
	}

	@Test
	public void testAddLocalization() {
		Refugee refugee = new Refugee();
		assertNull(refugee.getLocalizations());
		
		RefugeeLocalization localization = new RefugeeLocalization();
		localization.setLanguage("en");
		refugee.addLocalization(localization);
		assertNotNull(refugee.getLocalizations());
		assertTrue(refugee.getLocalizations().size() == 1);
	}

}
