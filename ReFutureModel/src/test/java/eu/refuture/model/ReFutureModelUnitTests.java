package eu.refuture.model;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import eu.refuture.model.country.CountryTest;
import eu.refuture.model.news.NewsTest;
import eu.refuture.model.poi.PointOfInterestTest;
import eu.refuture.model.poi.PointOfInterestTypeTest;
import eu.refuture.model.refugee.RefugeeTest;

/**
 * Defines a test suite for all model unit tests
 * 
 * @author Europe Without Borders e.V.
 * @version 1.0
 */
@RunWith(Suite.class)
@SuiteClasses({ CountryTest.class, NewsTest.class, PointOfInterestTest.class, PointOfInterestTypeTest.class,
		RefugeeTest.class })
public class ReFutureModelUnitTests {

}
