package eu.refuture.model.poi;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * @author Europe Without Borders e.V.
 * @version 1.0
 */
public class PointOfInterestTypeTest {

	@Test
	public void testAddLocalization() {
		PointOfInterestType poiType = new PointOfInterestType();
		assertNull(poiType.getLocalizations());

		PointOfInterestTypeLocalization localization = new PointOfInterestTypeLocalization();
		localization.setLang("en");
		poiType.addLocalization(localization);
		assertNotNull(poiType.getLocalizations());
		assertTrue(poiType.getLocalizations().size() == 1);
	}

}
