package eu.refuture.model.poi;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 * @author Europe Without Borders e.V.
 * @version 1.0
 */
public class PointOfInterestTest {

	@Test
	public void testAddLocalization() {
		PointOfInterest poi = new PointOfInterest();
		assertNull(poi.getLocalizations());

		PointOfInterestLocalization localization = new PointOfInterestLocalization();
		localization.setLanguage("en");
		poi.addLocalization(localization);
		assertNotNull(poi.getLocalizations());
		assertTrue(poi.getLocalizations().size() == 1);
	}

}
