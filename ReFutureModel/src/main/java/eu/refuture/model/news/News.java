package eu.refuture.model.news;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import eu.refuture.model.country.Country;
import eu.refuture.model.user.User;

/**
 * @author Europe Without Borders e.V.
 * @version 1.0
 */
@Entity
public class News {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	private double latitude;
	private double longitude;
	private int radius;
	private Country country; // TODO annotation necessary?
	@OneToMany(mappedBy = "news", cascade = CascadeType.PERSIST)
	private List<NewsLocalization> localizations;
	private boolean outdated = false;
	private User createdBy;
	private Date createdAt;
	private Date expirationDate;
	private News successor;
	private News predecessor;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public int getRadius() {
		return radius;
	}

	public void setRadius(int radius) {
		this.radius = radius;
	}

	public Country getCountry() {
		return country;
	}

	public void setCountry(Country country) {
		this.country = country;
	}

	public List<NewsLocalization> getLocalizations() {
		return localizations;
	}

	public void setLocalizations(List<NewsLocalization> localizations) {
		this.localizations = localizations;
	}

	public boolean isOutdated() {
		return outdated;
	}

	public void setOutdated(boolean outdated) {
		this.outdated = outdated;
	}

	public News getSuccessor() {
		return successor;
	}

	public void setSuccessor(News successor) {
		this.successor = successor;
	}

	public News getPredecessor() {
		return predecessor;
	}

	public void setPredecessor(News predecessor) {
		this.predecessor = predecessor;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}

	public void addLocalization(NewsLocalization localization) {
		if (localizations == null) {
			localizations = new ArrayList<NewsLocalization>();
		}

		localization.setNews(this);
		localizations.add(localization);
	}

}
