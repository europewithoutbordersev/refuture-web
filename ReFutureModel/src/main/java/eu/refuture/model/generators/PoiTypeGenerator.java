package eu.refuture.model.generators;

import java.util.ArrayList;
import java.util.List;

import eu.refuture.model.poi.PointOfInterestType;
import eu.refuture.model.poi.PointOfInterestTypeLocalization;

/**
 * Generates the poi-types
 * 
 * @author Europe Without Borders e.V.
 * @version 1.0
 */
public class PoiTypeGenerator {

	public static List<PointOfInterestType> getDefaultPoiTypes() {
		List<PointOfInterestType> poiTypes = new ArrayList<PointOfInterestType>();

		// Housing Type
		PointOfInterestType housingType = new PointOfInterestType();
		housingType.setId("housing");
		// de
		PointOfInterestTypeLocalization housingDeLocalization = new PointOfInterestTypeLocalization();
		housingDeLocalization.setName("Unterbringung");
		housingDeLocalization.setLang("de");
		housingDeLocalization.setType(housingType);
		housingType.addLocalization(housingDeLocalization);
		// en
		PointOfInterestTypeLocalization housingEnLocalization = new PointOfInterestTypeLocalization();
		housingEnLocalization.setName("Housing");
		housingEnLocalization.setLang("en");
		housingEnLocalization.setType(housingType);
		housingType.addLocalization(housingEnLocalization);
		// fr
		PointOfInterestTypeLocalization housingFrLocalization = new PointOfInterestTypeLocalization();
		housingFrLocalization.setName("Logement");
		housingFrLocalization.setLang("fr");
		housingFrLocalization.setType(housingType);
		housingType.addLocalization(housingFrLocalization);
		// ar
		PointOfInterestTypeLocalization housingArLocalization = new PointOfInterestTypeLocalization();
		housingArLocalization.setName("%D8%A7%D9%84%D8%A5%D9%82%D8%A7%D9%85%D8%A9");
		housingArLocalization.setLang("ar");
		housingArLocalization.setType(housingType);
		housingType.addLocalization(housingArLocalization);
		// add
		poiTypes.add(housingType);

		// medicine type
		PointOfInterestType medicineType = new PointOfInterestType();
		medicineType.setId("medicine");
		// de
		PointOfInterestTypeLocalization medicineDeLocalization = new PointOfInterestTypeLocalization();
		medicineDeLocalization.setName("Medizin");
		medicineDeLocalization.setLang("de");
		medicineDeLocalization.setType(medicineType);
		medicineType.addLocalization(medicineDeLocalization);
		// en
		PointOfInterestTypeLocalization medicineEnLocalization = new PointOfInterestTypeLocalization();
		medicineEnLocalization.setName("Medicine");
		medicineEnLocalization.setLang("en");
		medicineEnLocalization.setType(medicineType);
		medicineType.addLocalization(medicineEnLocalization);
		// fr
		PointOfInterestTypeLocalization medicineFrLocalization = new PointOfInterestTypeLocalization();
		medicineFrLocalization.setName("M%C3%A9dicine");
		medicineFrLocalization.setLang("fr");
		medicineFrLocalization.setType(medicineType);
		medicineType.addLocalization(medicineFrLocalization);
		// ar
		PointOfInterestTypeLocalization medicineArLocalization = new PointOfInterestTypeLocalization();
		medicineArLocalization.setName("%D8%AF%D9%88%D8%A7%D8%A1");
		medicineArLocalization.setLang("ar");
		medicineArLocalization.setType(medicineType);
		medicineType.addLocalization(medicineArLocalization);
		// add
		poiTypes.add(medicineType);

		// information type
		PointOfInterestType informationType = new PointOfInterestType();
		informationType.setId("information");
		// de
		PointOfInterestTypeLocalization informationDeLocalization = new PointOfInterestTypeLocalization();
		informationDeLocalization.setName("Information+und+Beratung");
		informationDeLocalization.setLang("de");
		informationDeLocalization.setType(informationType);
		informationType.addLocalization(informationDeLocalization);
		// en
		PointOfInterestTypeLocalization informationEnLocalization = new PointOfInterestTypeLocalization();
		informationEnLocalization.setName("Information%2FHelpdesk");
		informationEnLocalization.setLang("en");
		informationEnLocalization.setType(informationType);
		informationType.addLocalization(informationEnLocalization);

		PointOfInterestTypeLocalization informationFrLocalization = new PointOfInterestTypeLocalization();
		informationFrLocalization.setName("Information%2FConsultation");
		informationFrLocalization.setLang("fr");
		informationFrLocalization.setType(informationType);
		informationType.addLocalization(informationFrLocalization);

		PointOfInterestTypeLocalization informationArLocalization = new PointOfInterestTypeLocalization();
		informationArLocalization
				.setName("%D9%85%D8%B9%D9%84%D9%88%D9%85%D8%A7%D8%AA+%D9%88+%D9%86%D8%B5%D9%8A%D8%AD%D8%A9");
		informationArLocalization.setLang("ar");
		informationArLocalization.setType(informationType);
		informationType.addLocalization(informationArLocalization);
		// add
		poiTypes.add(informationType);

		return poiTypes;
	}
}
