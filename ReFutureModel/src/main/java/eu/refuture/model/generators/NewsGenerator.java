package eu.refuture.model.generators;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import eu.refuture.model.news.News;
import eu.refuture.model.news.NewsLocalization;

/**
 * Generates Sample News
 * 
 * @author Europe Without Borders e.V.
 * @version 1.0
 */
public class NewsGenerator {

	public static List<News> getSampleNews() {
		List<News> defaultNews = new ArrayList<News>();

		for (int i = 1; i <= 100; i++) {
			News newNews = new News();
			newNews.setCreatedAt(new Date(1450120085000L + (180000 * i)));
			// en
			NewsLocalization enLocalization = new NewsLocalization();
			enLocalization.setHeading("Welcome+" + i);
			enLocalization.setBody("Welcome%0AThis+is+the+message%0A" + i);
			enLocalization.setLanguage("en");
			enLocalization.setNews(newNews);
			newNews.addLocalization(enLocalization);
			// de
			NewsLocalization deLocalization = new NewsLocalization();
			deLocalization.setHeading("Willkommen+" + i);
			deLocalization.setBody("Willkommen%0ADies+ist+die+Nachricht%0A" + i);
			deLocalization.setLanguage("de");
			deLocalization.setNews(newNews);
			newNews.addLocalization(deLocalization);
			// fr
			NewsLocalization frLocalization = new NewsLocalization();
			frLocalization.setHeading("Bienvenu+" + i);
			frLocalization.setBody("Bienvenu%0ATel+est+le+message%0A" + i);
			frLocalization.setLanguage("fr");
			frLocalization.setNews(newNews);
			newNews.addLocalization(frLocalization);
			// ar
			NewsLocalization arLocalization = new NewsLocalization();
			arLocalization.setHeading(i + "+%D8%AA%D8%B1%D8%AD%D9%8A%D8%A8");
			arLocalization.setBody(
					"%D8%AA%D8%B1%D8%AD%D9%8A%D8%A8%0A%D9%87%D8%B0%D9%87+%D9%87%D9%8A+%D8%A7%D9%84%D8%B1%D8%B3%D8%A7%D9%84%D8%A9%0A"
							+ i);
			arLocalization.setLanguage("ar");
			arLocalization.setNews(newNews);
			newNews.addLocalization(arLocalization);
			// add
			defaultNews.add(newNews);
		}

		return defaultNews;
	}

}
