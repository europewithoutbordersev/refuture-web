package eu.refuture.model.generators;

import java.util.ArrayList;
import java.util.List;

import eu.refuture.model.country.Country;
import eu.refuture.model.country.CountryLocalization;

/**
 * Generates the Countries.
 * 
 * @author Europe Without Borders e.V.
 * @version 1.0
 */
public class CountriesGenerator {

	public static List<Country> getDefaultCountries() {
		List<Country> countries = new ArrayList<Country>();

		Country germany = new Country();
		germany.setId("DE");
		CountryLocalization germanyLocal = new CountryLocalization();
		germanyLocal.setLang("en");
		germanyLocal.setName("Germany");
		germanyLocal.setCountry(germany);
		germany.addLocalization(germanyLocal);
		countries.add(germany);

		Country andorra = new Country();
		andorra.setId("AD");
		countries.add(andorra);

		Country austria = new Country();
		austria.setId("AT");
		countries.add(austria);

		Country belgium = new Country();
		belgium.setId("BE");
		countries.add(belgium);

		Country croatia = new Country();
		croatia.setId("HR");
		countries.add(croatia);

		Country cyprus = new Country();
		cyprus.setId("CY");
		countries.add(cyprus);

		Country czechRepublic = new Country();
		czechRepublic.setId("CZ");
		countries.add(czechRepublic);

		Country denmark = new Country();
		denmark.setId("DK");
		countries.add(denmark);

		Country estonia = new Country();
		estonia.setId("EE");
		countries.add(estonia);

		Country ireland = new Country();
		ireland.setId("IE");
		countries.add(ireland);

		Country greece = new Country();
		greece.setId("GR");
		countries.add(greece);

		Country spain = new Country();
		spain.setId("ES");
		countries.add(spain);

		Country france = new Country();
		france.setId("FR");
		countries.add(france);

		Country latvia = new Country();
		latvia.setId("LV");
		countries.add(latvia);

		Country italy = new Country();
		italy.setId("IT");
		countries.add(italy);

		Country lithuania = new Country();
		lithuania.setId("LT");
		countries.add(lithuania);

		Country luxembourg = new Country();
		luxembourg.setId("LU");
		countries.add(luxembourg);

		Country hungary = new Country();
		hungary.setId("HU");
		countries.add(hungary);

		Country malta = new Country();
		malta.setId("MT");
		countries.add(malta);

		Country netherlands = new Country();
		netherlands.setId("NL");
		countries.add(netherlands);

		Country poland = new Country();
		poland.setId("PL");
		countries.add(poland);

		Country portugal = new Country();
		portugal.setId("PT");
		countries.add(portugal);

		Country romania = new Country();
		romania.setId("RO");
		countries.add(romania);

		Country slovenia = new Country();
		slovenia.setId("SI");
		countries.add(slovenia);

		Country slovakia = new Country();
		slovakia.setId("SK");
		countries.add(slovakia);

		Country finland = new Country();
		finland.setId("FI");
		countries.add(finland);

		Country sweden = new Country();
		sweden.setId("SE");
		countries.add(sweden);

		Country unitedKingdom = new Country();
		unitedKingdom.setId("UK");
		countries.add(unitedKingdom);

		return countries;
	}
}
