package eu.refuture.model.generators;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Random;

import eu.refuture.model.poi.PointOfInterest;
import eu.refuture.model.poi.PointOfInterestLocalization;
import eu.refuture.model.poi.PointOfInterestType;
import eu.refuture.model.poi.PointOfInterestTypeLocalization;

/**
 * Generates sample POIs
 * 
 * @author Europe Without Borders e.V.
 * @version 1.0
 */
public class PoiGenerator {

	public static List<PointOfInterest> getSamplePois(Collection<PointOfInterestType> types) {
		List<PointOfInterest> pois = new ArrayList<PointOfInterest>();

		for (PointOfInterestType type : types) {
			for (int i = 1; i <= 10; i++) {
				PointOfInterest pointOfInterest = new PointOfInterest();
				pointOfInterest.setCreatedAt(new Date());
				if (new Random().nextBoolean()) {
					pointOfInterest.setLatitude(48.0501442 + (new Random().nextDouble() * 2));
				} else {
					pointOfInterest.setLatitude(48.0501442 - (new Random().nextDouble() * 2));
				}
				if (new Random().nextBoolean()) {
					pointOfInterest.setLongitude(8.2014192 + (new Random().nextDouble() * 2));
				} else {
					pointOfInterest.setLongitude(8.2014192 - (new Random().nextDouble() * 2));
				}

				pointOfInterest.setRatio(new Random().nextInt(101) - 1);
				pointOfInterest.setType(type);
				for (PointOfInterestTypeLocalization typeLocal : type.getLocalizations()) {
					PointOfInterestLocalization localization = new PointOfInterestLocalization();
					localization.setLanguage(typeLocal.getLang());
					localization.setName(typeLocal.getName() + "+" + i);
					localization.setDescription(String.valueOf(i));
					localization.setPointOfInterest(pointOfInterest);
					pointOfInterest.addLocalization(localization);
				}

				pois.add(pointOfInterest);
			}
		}

		return pois;
	}

}
