package eu.refuture.model.generators;

import java.util.ArrayList;
import java.util.List;

import eu.refuture.model.user.User;
import eu.refuture.model.user.UserRoles;

/**
 * Generates the default users
 * 
 * @author Europe Without Borders e.V.
 * @version 1.0
 */
public class UsersGenerator {
	public static List<User> getDefaultUsers() {
		List<User> defaultUsers = new ArrayList<User>();

		// default admin user
		User admin = new User();
		admin.seteMail("admin@example.org");
		admin.setName("Administrator");
		admin.setPassword("admin");
		admin.setPostalAddress("Furtwangen");
		admin.setRole(UserRoles.ADMINISTRATOR);
		defaultUsers.add(admin);

		return defaultUsers;
	}
}
