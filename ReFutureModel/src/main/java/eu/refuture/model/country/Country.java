package eu.refuture.model.country;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

/**
 * @author Europe Without Borders e.V.
 * @version 1.0
 */
@Entity
public class Country {
	@Id
	@Column(length = 2)
	private String id;
	@OneToMany(mappedBy = "country", cascade = CascadeType.PERSIST)
	private List<CountryLocalization> localizations;
	private int accommodationCapacity; // Unterbringungskapazitaeten
	private int occupancyRate;
	private int averageLengthOfProceedings; // in months
	private int unemploymentRate;
	private boolean languageIsRequired;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public List<CountryLocalization> getLocalizations() {
		return localizations;
	}

	public void setLocalizations(List<CountryLocalization> localizations) {
		this.localizations = localizations;
	}

	public int getAccommodationCapacity() {
		return accommodationCapacity;
	}

	public void setAccommodationCapacity(int accommodationCapacity) {
		this.accommodationCapacity = accommodationCapacity;
	}

	public int getOccupancyRate() {
		return occupancyRate;
	}

	public void setOccupancyRate(int occupancyRate) {
		this.occupancyRate = occupancyRate;
	}

	public int getAverageLengthOfProceedings() {
		return averageLengthOfProceedings;
	}

	public void setAverageLengthOfProceedings(int averageLengthOfProceedings) {
		this.averageLengthOfProceedings = averageLengthOfProceedings;
	}

	public int getUnemploymentRate() {
		return unemploymentRate;
	}

	public void setUnemploymentRate(int unemploymentRate) {
		this.unemploymentRate = unemploymentRate;
	}

	public boolean isLanguageIsRequired() {
		return languageIsRequired;
	}

	public void setLanguageIsRequired(boolean languageIsRequired) {
		this.languageIsRequired = languageIsRequired;
	}

	public void addLocalization(CountryLocalization localization) {
		if (localizations == null) {
			localizations = new ArrayList<CountryLocalization>();
		}

		localizations.add(localization);
	}

}
