package eu.refuture.model.country;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 * @author Europe Without Borders e.V.
 * @version 1.0
 */
@Entity
public class CountryLocalization {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	@ManyToOne
	private Country country;
	@Column(length = 2)
	private String lang;
	private String name;
	private String workPermitRequirements;
	private String rulesForFamilyReunification;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Country getCountry() {
		return country;
	}

	public void setCountry(Country country) {
		this.country = country;
	}

	public String getLang() {
		return lang;
	}

	public void setLang(String lang) {
		this.lang = lang;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getWorkPermitRequirements() {
		return workPermitRequirements;
	}

	public void setWorkPermitRequirements(String workPermitRequirements) {
		this.workPermitRequirements = workPermitRequirements;
	}

	public String getRulesForFamilyReunification() {
		return rulesForFamilyReunification;
	}

	public void setRulesForFamilyReunification(String rulesForFamilyReunification) {
		this.rulesForFamilyReunification = rulesForFamilyReunification;
	}

}
