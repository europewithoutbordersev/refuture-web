package eu.refuture.model.user;

/**
 * Defines different user roles
 * 
 * @author Europe Without Borders e.V.
 * @version 1.0
 */
// will not be persisted
public class UserRoles {

	public static final String ADMINISTRATOR = "administrator";
	public static final String TRANSLATOR = "translator";
	public static final String INTERNATIONAL_ORGANISATION = "international";
	public static final String REGIONAL_ORGANISATION = "regional";
}
