package eu.refuture.model.refugee;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import eu.refuture.model.country.Country;

/**
 * @author Europe Without Borders e.V.
 * @version 1.0
 */
@Entity
public class Refugee {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	private String gcmCode;
	private Country startCountry; // TODO check if annotation needed
	private Country targetCountry; // TODO check if annotation needed
	private int radius;
	private int entourage;
	private int familyMembersFollowing;
	private int age;
	private char gender;
	private String education;
	@OneToMany(mappedBy = "refugee", cascade = CascadeType.PERSIST)
	private List<RefugeeLocalization> localizations;
	@OneToMany(mappedBy = "refugee", cascade = CascadeType.PERSIST)
	private List<RefugeePosition> positions;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getGcmCode() {
		return gcmCode;
	}

	public void setGcmCode(String gcmCode) {
		this.gcmCode = gcmCode;
	}

	public Country getStartCountry() {
		return startCountry;
	}

	public void setStartCountry(Country startCountry) {
		this.startCountry = startCountry;
	}

	public Country getTargetCountry() {
		return targetCountry;
	}

	public void setTargetCountry(Country targetCountry) {
		this.targetCountry = targetCountry;
	}

	public int getRadius() {
		return radius;
	}

	public void setRadius(int radius) {
		this.radius = radius;
	}

	public int getEntourage() {
		return entourage;
	}

	public void setEntourage(int entourage) {
		this.entourage = entourage;
	}

	public int isFamilyMembersFollowing() {
		return familyMembersFollowing;
	}

	public void setFamilyMembersFollowing(int familyMembersFollowing) {
		this.familyMembersFollowing = familyMembersFollowing;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public char getGender() {
		return gender;
	}

	public void setGender(char gender) {
		this.gender = gender;
	}

	public String getEducation() {
		return education;
	}

	public void setEducation(String education) {
		this.education = education;
	}

	public List<RefugeeLocalization> getLocalizations() {
		return localizations;
	}

	public void setLocalizations(List<RefugeeLocalization> localizations) {
		this.localizations = localizations;
	}

	public List<RefugeePosition> getPositions() {
		return positions;
	}

	public void setPositions(List<RefugeePosition> positions) {
		this.positions = positions;
	}

	public void addPosition(RefugeePosition position) {
		if (getPositions() == null) {
			positions = new ArrayList<RefugeePosition>();
		}

		positions.add(position);
	}

	public void addLocalization(RefugeeLocalization localization) {
		if (getLocalizations() == null) {
			localizations = new ArrayList<RefugeeLocalization>();
		}

		localizations.add(localization);
	}

}
