package eu.refuture.model.refugee;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 * @author Europe Without Borders e.V.
 * @version 1.0
 */
@Entity
public class RefugeeLocalization {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	@ManyToOne
	private Refugee refugee;
	@Column(length = 2)
	private String language;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Refugee getRefugee() {
		return refugee;
	}

	public void setRefugee(Refugee refugee) {
		this.refugee = refugee;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

}
