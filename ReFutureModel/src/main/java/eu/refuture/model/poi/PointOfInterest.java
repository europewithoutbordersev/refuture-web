package eu.refuture.model.poi;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import eu.refuture.model.user.User;

/**
 * @author Europe Without Borders e.V.
 * @version 1.0
 */
@Entity
public class PointOfInterest {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	private double latitude;
	private double longitude;
	private int radius;
	private int ratio;
	private boolean createdByGovernment; // TODO is this field really necessary?
	private User createdBy;
	private Date createdAt;
	@OneToMany(mappedBy = "pointOfInterest", cascade = CascadeType.PERSIST)
	private List<PointOfInterestLocalization> localizations;
	@ManyToOne
	private PointOfInterestType type;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public int getRadius() {
		return radius;
	}

	public void setRadius(int radius) {
		this.radius = radius;
	}

	public int getRatio() {
		return ratio;
	}

	public void setRatio(int ration) {
		this.ratio = ration;
	}

	public boolean isCreatedByGovernment() {
		return createdByGovernment;
	}

	public void setCreatedByGovernment(boolean createdByGovernment) {
		this.createdByGovernment = createdByGovernment;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public List<PointOfInterestLocalization> getLocalizations() {
		return localizations;
	}

	public void setLocalizations(List<PointOfInterestLocalization> internationalizations) {
		this.localizations = internationalizations;
	}

	public PointOfInterestType getType() {
		return type;
	}

	public void setType(PointOfInterestType type) {
		this.type = type;
	}

	public void addLocalization(PointOfInterestLocalization localization) {
		if (localizations == null) {
			localizations = new ArrayList<PointOfInterestLocalization>();
		}

		localizations.add(localization);
	}

}
