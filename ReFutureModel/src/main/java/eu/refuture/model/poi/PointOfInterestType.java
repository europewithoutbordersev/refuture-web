package eu.refuture.model.poi;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

/**
 * @author Europe Without Borders e.V.
 * @version 1.0
 */
@Entity
public class PointOfInterestType {
	@Id
	private String id;
	@OneToMany(mappedBy = "type", cascade = CascadeType.PERSIST)
	private List<PointOfInterest> pointsOfInterest;
	@OneToMany(mappedBy = "type", cascade = CascadeType.PERSIST)
	private List<PointOfInterestTypeLocalization> localizations;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public List<PointOfInterest> getPointsOfInterest() {
		return pointsOfInterest;
	}

	public void setPointsOfInterest(List<PointOfInterest> pointsOfInterest) {
		this.pointsOfInterest = pointsOfInterest;
	}

	public List<PointOfInterestTypeLocalization> getLocalizations() {
		return localizations;
	}

	public void setLocalizations(List<PointOfInterestTypeLocalization> localizations) {
		this.localizations = localizations;
	}

	public void addLocalization(PointOfInterestTypeLocalization localization) {
		if (getLocalizations() == null) {
			localizations = new ArrayList<PointOfInterestTypeLocalization>();
		}

		localizations.add(localization);
	}

}
