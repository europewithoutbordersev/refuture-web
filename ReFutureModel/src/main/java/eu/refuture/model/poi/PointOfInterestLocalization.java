package eu.refuture.model.poi;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 * @author Europe Without Borders e.V.
 * @version 1.0
 */
@Entity
public class PointOfInterestLocalization implements Comparable<PointOfInterestLocalization> {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	@ManyToOne
	private PointOfInterest pointOfInterest;
	@Column(length = 2)
	private String language;
	private String name;
	private String description;
	private String capacity;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public PointOfInterest getPointOfInterest() {
		return pointOfInterest;
	}

	public void setPointOfInterest(PointOfInterest pointOfInterest) {
		this.pointOfInterest = pointOfInterest;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCapacity() {
		return capacity;
	}

	public void setCapacity(String capacity) {
		this.capacity = capacity;
	}

	@Override
	public int compareTo(PointOfInterestLocalization o) {
		return language.compareTo(o.language);
	}

}
