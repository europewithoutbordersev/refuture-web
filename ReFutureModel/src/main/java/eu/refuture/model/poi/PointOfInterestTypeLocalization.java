package eu.refuture.model.poi;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 * @author Europe Without Borders e.V.
 * @version 1.0
 */
@Entity
public class PointOfInterestTypeLocalization implements Comparable<PointOfInterestTypeLocalization> {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	@ManyToOne
	private PointOfInterestType type;
	@Column(length = 2)
	private String lang;
	private String name;
	private String description;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public PointOfInterestType getType() {
		return type;
	}

	public void setType(PointOfInterestType type) {
		this.type = type;
	}

	public String getLang() {
		return lang;
	}

	public void setLang(String lang) {
		this.lang = lang;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public int compareTo(PointOfInterestTypeLocalization o) {
		return lang.compareTo(o.lang);
	}

}
